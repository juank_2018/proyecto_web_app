Proyecto final del Grado Superior de Desarrollo de Aplicaciones Multiplataforma.

El proyecto consiste en una aplicacion web y una app android para que un restaurante pueda mostar la carta y comunicarse con los clientes para ofrecer ofertas.

La aplicación web está desarrollada en Java usando el framework Spring MVC. Para la capa de persistencia usé Hibernate y la base de datos se aloja en MySQL.
Utiliza Firebase para enviar y recibir mensajes con la app Android.