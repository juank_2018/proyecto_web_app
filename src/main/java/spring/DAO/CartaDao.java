/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.DAO;

import java.util.List;
import spring.model.Carta;

/**
 * Interfaz con los métodos a implementar para la capa DAO
 *
 * @author jcpm0
 */
public interface CartaDao {

  /**
   * Método añadir carta, introduce una nueva carta en la bbdd
   *
   * @param c objeto Carta.
   */
  public void addCarta(Carta c);

  /**
   * Método para actualizar una carta
   *
   * @param c Objeto Carta
   */
  public void updateCarta(Carta c);

  /**
   * Método para listar cartas
   *
   * @return Devuelve una lista de cartas
   */
  public List<Carta> listCartas();

  /**
   * Método para devolver una lista de cartas con paginación
   *
   * @param offset Desplazamiento sobre el total de registros
   * @param maxResults Número máximo de resultados a devolver
   * @return Devuelve una lista con las cartas
   */
  public List<Carta> listCartas(Integer offset, Integer maxResults);

  /**
   * Método para obtener una carta por su id
   *
   * @param id El id de la carta
   * @return Devuelve un objeto carta
   */
  public Carta getCartaById(int id);

  /**
   * Método para eliminar una carta
   *
   * @param id El id de la carta
   */
  public void removeCarta(int id);
}
