/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.DAO;

import java.util.List;
import spring.model.Clientes;

/**
 * Interfaz con la definición de los métodos para la capa DAO de la tabla
 * Clientes
 *
 * @author jcpm0
 */
public interface ClientesDao {

  /**
   * Método para añadir un Cliente
   *
   * @param c Obejto Clientes para insertar
   */
  public void addClientes(Clientes c);

  /**
   * Método para actualizar un cliente
   *
   * @param c Objeto Clientes para actualizar
   */
  public void updateClientes(Clientes c);

  /**
   * Método para obtener la lista de clientes
   *
   * @return Devuelve una lista de objetos Clientes
   */
  public List<Clientes> listClientes();

  /**
   * Método para obtener un cliente por su id
   *
   * @param id El id del cliente
   * @return Devuelve un objeto Clientes
   */
  public Clientes getClientesById(int id);

  /**
   * Mátodo para eliminar un cliente
   *
   * @param id id del cliente
   */
  public void removeClientes(int id);

  /**
   * Método para obtener un cliente por su email
   *
   * @param email String con el email del cliente
   * @return Objeto cliente o null
   */
  public Clientes findClienteByEmail(String email);
}
