/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.DAO;

import java.util.List;
import spring.model.Dispositivos;

/**
 * Interface con los métodos del DAO para la tabala dispositivos
 *
 * @author jcpm0
 */
public interface DispositivosDao {

  /**
   * Método para añadir dispositivo
   *
   * @param c Objeto Dispositivos
   */
  public void addDispositivos(Dispositivos c);

  /**
   * Método para actualizar un dispositivo
   *
   * @param c Objeto Dispositivos
   */
  public void updateDispositivos(Dispositivos c);

  /**
   * Método para obtener la lista de los dispositivos
   *
   * @return Devuelve una lista con los dispositivos
   */
  public List<Dispositivos> listDispositivos();

  /**
   * Obtiene un dispositivos por su id
   *
   * @param id el id del dispositivo
   * @return Devuelve objeto Dispositivos
   */
  public Dispositivos getDispositivosById(int id);

  /**
   * Elimina un dispositivo
   *
   * @param id el id del dispositivo
   */
  public void removeDispositivos(int id);

}
