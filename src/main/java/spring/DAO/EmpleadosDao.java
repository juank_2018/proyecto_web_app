package spring.DAO;

import java.util.List;
import spring.model.Empleados;

/**
 * Interface con los métodos para el DAO de Empleados
 *
 * @author jcpm0
 */
public interface EmpleadosDao {

  /**
   * Inserta un Empleados
   *
   * @param c Objeto Empleados
   */
  public void addEmpleados(Empleados c);

  /**
   * Actualiza un empleado
   *
   * @param c Objeto Empleados
   */
  public void updateEmpleados(Empleados c);

  /**
   * Obtiene la lista de empleados
   *
   * @return Devuelve una lista con los empleados
   */
  public List<Empleados> listEmpleados();

  /**
   * Obtiene un empleado por su id
   *
   * @param id el id del empleado
   * @return Devuelve un objeto Empleados
   */
  public Empleados getEmpleadosById(int id);

  /**
   * Elimina un empleado por su id
   *
   * @param id el id del empleado
   */
  public void removeEmpleados(int id);
  
  public List<Empleados> listarEmpleados(Integer offset, Integer maxResults);

}
