package spring.DAO;

import java.util.List;
import spring.model.Menu;

/**
 *
 * @author jcpm0
 */
public interface MenuDao {

  /**
   * añade un Menu
   *
   * @param c Objeto Menu
   */
  public void addMenu(Menu c);

  /**
   * Actualiza un menu
   *
   * @param c Objeto menu
   */
  public void updateMenu(Menu c);

  /**
   * Obtiene la lista de Menus
   *
   * @return Lista de objetos Menu
   */
  public List<Menu> listMenus();

  /**
   * Obtiene un menu por el id
   *
   * @param id el id del menu
   * @return Obejto Menu
   */
  public Menu getMenuById(int id);

  /**
   * Elimina un menu
   *
   * @param id id del menu
   */
  public void removeMenu(int id);

  /**
   * Obtiene la lista de menú con paginación
   *
   * @param offset Desplazamiento sobre el total de registros
   * @param maxResults Máximo de resultados de la consulta
   * @return Lista de objetos Menu
   */
  public List<Menu> listMenus(Integer offset, Integer maxResults);

  /**
   * Obtiene un menu por la fecha
   *
   * @param date String con la fecha
   * @return objeto menu
   */
  public Menu getMenuByDate(String date);

}
