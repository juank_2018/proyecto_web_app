/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.DAO;

import java.util.List;
import spring.model.Notif_sistema;

/**
 *
 * @author jcpm0
 */
public interface Notif_sistemaDao {

  /**
   * Añade una notificacion de sistema
   *
   * @param c Objeto Notif_sistema
   */
  public void addNotif_sistema(Notif_sistema c);

  /**
   * Acutaliza una notificación de sistema
   *
   * @param c Objeto Notif_sistema
   */
  public void updateNotif_sistema(Notif_sistema c);

  /**
   * Obtiene una lista de las notificaciones
   *
   * @return Lista de objetos Notif_sistema
   */
  public List<Notif_sistema> listNotif_sistemas();

  /**
   * Obitene una notificacion por su id
   *
   * @param id el id de la notificación
   * @return Objeto notif_sistema
   */
  public Notif_sistema getNotif_sistemaById(int id);

  /**
   * Elimina un notifiación
   *
   * @param id id de la notificación
   */
  public void removeNotif_sistema(int id);
}
