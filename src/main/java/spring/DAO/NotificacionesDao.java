/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.DAO;

import java.util.List;
import spring.model.Notificaciones;

/**
 *
 * @author jcpm0
 */
public interface NotificacionesDao {

  /**
   * Añade una norificación
   *
   * @param c Objeto Notificaciones
   */
  public void addNotificaciones(Notificaciones c);

  /**
   * Actualiza una notificación
   *
   * @param c Objeto Notificaciones
   */
  public void updateNotificaciones(Notificaciones c);

  /**
   * Obtiene un listado de las notificaciones
   *
   * @return Lista de objetos Notificaciones
   */
  public List<Notificaciones> listNotificaciones();

  /**
   * Obtiene
   *
   * @param id
   * @return
   */
  public Notificaciones getNotificacionesById(int id);

  /**
   *
   * @param id
   */
  public void removeNotificaciones(int id);

  /**
   *
   * @param tipo
   * @return
   */
  public List<Notificaciones> listNotificacionesPorTipo(boolean tipo);

  /**
   *
   * @param cliente
   * @param offset
   * @param maxResults
   * @return
   */
  public List<Notificaciones> listNotificaciones(boolean cliente, Integer offset, Integer maxResults);
}
