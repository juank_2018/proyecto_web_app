/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.DAO;

import java.util.List;
import spring.model.Tipo_plato;

/**
 *
 * @author jcpm0
 */
public interface Tipo_platoDao {

  /**
   *
   * @param c
   */
  public void addTipo_plato(Tipo_plato c);

  /**
   *
   * @param c
   */
  public void updateTipo_plato(Tipo_plato c);

  /**
   *
   * @return
   */
  public List<Tipo_plato> listTipo_platos();

  /**
   *
   * @param id
   * @return
   */
  public Tipo_plato getTipo_platoById(int id);

  /**
   *
   * @param id
   */
  public void removeTipo_plato(int id);
}
