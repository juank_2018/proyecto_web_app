/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.REST;

import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import spring.model.Carta;
import spring.model.Platos;
import spring.utils.SeccionCartaREST;
import spring.model.Tipo_plato;
import spring.service.CartaService;
import spring.service.PlatosService;
import spring.service.Tipo_platoService;

/**
 *
 * @author jcpm0
 */
@RestController
public class CartaRESTController {

  private Logger LOG = Logger.getLogger(CartaRESTController.class);
  @Autowired
  private CartaService cartaService;
  @Autowired
  private PlatosService platosService;
  @Autowired
  private Tipo_platoService tipoPlatoService;

  /**
   * Devuelve la lista de cartas como JSON
   *
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/carta", method = RequestMethod.GET)
  public ResponseEntity<List<Carta>> listCarta() {
    //obtiene la lista de cartas
    List<Carta> carta = cartaService.listCartas();
    LOG.info("entra. " + carta.toString());
    //si la lista está vacía devuelve el código HTTP NO_CONTENT
    if (carta.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    //si no está vacia, devuelve la lista y código 200
    return new ResponseEntity<>(carta, HttpStatus.OK);
  }

  /**
   * Gestiona la petición de listar los platos de una carta desde la App
   *
   * @param id El id de la carta
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/carta/{id}", method = RequestMethod.GET)
  public ResponseEntity<List<SeccionCartaREST>> listCarta(@PathVariable("id") int id) {
    //obtiene los platos que están en la carta
    List<Platos> platosCarta = platosService.platosQueEstanEnCarta(id);
    //obtiene la lista de tipos de platos
    List<Tipo_plato> tipos = tipoPlatoService.listTipo_platos();
    //crea una lista de objeto SeccionCartaREST

    List<SeccionCartaREST> resultado = new ArrayList<>();
    //si la lista de platos está vacia devuelvo error 404
    if (platosCarta == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      /*Para cada tipo de plato creo un objeto SeccionCartaREST, luego recorro la lista de 
      platos para separar los de la sección y la meto en la lista de objetos SeccionCartaREST */
      for (Tipo_plato tp : tipos) {
        SeccionCartaREST seccion = new SeccionCartaREST();
        List<Platos> platosSeccion = new ArrayList<>();
        seccion.setPlatosSeccion(platosSeccion);
        seccion.setIdTipoPlato(tp.getIdTipo());
        seccion.setTipoPlato(tp.getTipo());
        for (Platos p : platosCarta) {
          int tipoDelPlato = p.getTipo_plato().get(0).getIdTipo();
          if (tipoDelPlato == tp.getIdTipo()) {

            seccion.getPlatosSeccion().add(p);
          }
        }
        if (!seccion.getPlatosSeccion().isEmpty()) {
          resultado.add(seccion);
        }

      }
    }
    return new ResponseEntity<>(resultado, HttpStatus.OK);
  }

}
