/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.REST;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import spring.model.Clientes;
import spring.model.Dispositivos;
import spring.utils.SimpleResponse;
import spring.model.Usuarios;
import spring.service.ClientesService;
import spring.service.DispositivosService;
import spring.service.EnvioMailService;
import spring.service.UsuariosService;

/**
 * Clase para gestionar las peticiones al servicio REST en relación a la gestión
 * de clientes
 *
 * @author jcpm0
 */
@RestController
public class ClienteRESTController {

  private final Logger LOG = Logger.getLogger(ClienteRESTController.class);

  @Autowired
  private ClientesService clientesService;
  @Autowired
  private EnvioMailService envioMailService;
  @Autowired
  private UsuariosService usuariosService;
  @Autowired
  private DispositivosService dispositivosService;

  /**
   * Método que gestiona el envío de contraseña
   *
   * @param email String con el email del cliente
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/enviaContrasena", method = RequestMethod.GET)
  public ResponseEntity<SimpleResponse> enviaCotraseña(@RequestParam("email") String email) {

    String email_limpio = null;
    SimpleResponse sr = new SimpleResponse();
    /*Antes que nada preparar la cadena email para que tenga comillas simples*/
    if (email.startsWith("\"")) {
      email_limpio = email.replace("\"", "\'");
    } else {
      email_limpio = "\'" + email + "\'";
    }

    try {
      //buscamos el cliente
      Clientes c = clientesService.findClienteByEmail(email_limpio);
      if (c == null) {
        //si no existe devuelvo error 404
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      } else {
        //si existe mando el correo con la contraseña a través del servicio de correo
        //y devuelvo OK
        envioMailService.send(c.getEmail(),
                "pruebas.proyectoDAM@gmail.com",
                "Aquí tienes tu contraseña",
                "La contraseña para la aplicación es: " + c.getPaswd());
        sr.setError(false);
        sr.setMessage("envio correcto");
        return new ResponseEntity<>(sr, HttpStatus.OK);
      }
    } catch (Exception e) {
      //si pasa algo raro error 500
      LOG.info(e.toString());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

    }

  }

  /**
   * Comprueba si existe el cliente
   *
   * @param email String con el email
   * @param passwd String con la passwd
   * @param token String con el token del dispositivo del cliente
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/cliente", method = RequestMethod.GET)
  public ResponseEntity<Clientes> compruebaCliente(@RequestParam("email") String email,
          @RequestParam("passwd") String passwd,
          @RequestParam("token") String token) {
    LOG.info(passwd);
    LOG.info(email);
    //Se formatea el email y passwd con comillas simples para la consulta
    String email_limpio = null;
    String passwd_limpio = null;
    if (email.startsWith("\"")) {
      email_limpio = email.replace("\"", "\'");
    } else {
      email_limpio = "\'" + email + "\'";
    }
    if (passwd.startsWith("\"")) {
      passwd_limpio = passwd.replace("\"", "");
    } else {
      passwd_limpio = passwd;
    }

    LOG.info(email_limpio);
    LOG.info("pas_limpio: " + passwd_limpio);
    //buscamos el cliente
    Clientes c = clientesService.findClienteByEmail(email_limpio);
    //si no exite, error 404                                                                                           
    if (c == null) {
      LOG.info("c = null");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      //si existe comprobamos la contraseña
      if (c.getPaswd().equals(passwd_limpio)) {
        LOG.info("contraseña valida");
        /*Si el cliente se ha autentificado ok, comprobamos que el token que ha enviado exista ya o no en la bbdd
        si no existe es que es un terminal nuevo o ha reinstalado la app y lo tenemos que incliur*/
        //si el token no es vacio
        if (!token.isEmpty()) {
          //obtenemos cliente
          Clientes c1 = clientesService.findClienteByEmail(email_limpio);
          boolean existe = false;
          //obtenemos los dispositivos del cliente
          Set<Dispositivos> dispositivos = c1.getDispositivos();
          //buscamos si ya existe, si existe no hacemos nada
          for (Dispositivos d : dispositivos) {
            if (d.getToken().equals(token)) {
              existe = true;
            }
          }
          //si no existe se inserta el nuevo dispositivo
          if (!existe) {
            Dispositivos nuevo_dispositivo = new Dispositivos();
            nuevo_dispositivo.setToken(token);
            nuevo_dispositivo.setCliente(c1);
            c1.getDispositivos().add(nuevo_dispositivo);

            dispositivosService.updateDispositivos(nuevo_dispositivo);
            LOG.error(c1.toString());
          }
        }
        //cliente ok, devolvemos 200
        return new ResponseEntity<>(c, HttpStatus.OK);
      } else {
        //contraseña incorrecta, devuelvo 403
        LOG.info("contraseña no valida");
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
      }
    }

  }

  /**
   * Gestiona la creación de un nuevo cliente
   *
   * @param email String con el mail del cliente
   * @param passwd String con el passwd del cliente
   * @param dispositivo String con el token del dispositivo
   * @param nombre String con el nombre del cliente
   * @param apellido1 String con el primer apellido
   * @param apellido2 String con el segundo apellido
   * @param fecha String con la fecha de nacimiento
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/crearCliente", method = RequestMethod.POST)
  public ResponseEntity<Clientes> crearCliente(
          @RequestParam("email") String email,
          @RequestParam("passwd") String passwd,
          @RequestParam("token") String dispositivo,
          @RequestParam("nombre") String nombre,
          @RequestParam("apellido1") String apellido1,
          @RequestParam("apellido2") String apellido2,
          @RequestParam("fecha") String fecha) {

    LOG.info(dispositivo);
    String email_limpio = null;
    //formateo del email con comillas simples
    if (email.startsWith("\"")) {
      email_limpio = email.replace("\"", "\'");
    } else {
      email_limpio = "\'" + email + "\'";
    }
    LOG.info("crearCliente " + email_limpio);
    System.out.println(email_limpio);
    //cliente a insertar
    Clientes c = new Clientes();
    //si no existe lo creo
    if (clientesService.findClienteByEmail(email_limpio) == null) {

      c.setEmail(email);
      c.setPaswd(passwd);

      Usuarios u = new Usuarios();
      u.setNombre(nombre);
      u.setApellido1(apellido1);
      u.setApellido2(apellido2);
      u.setFechanacimiento(fecha);
      usuariosService.addUsuarios(u);
      c.setUsuario(u);
      Set<Dispositivos> dispo = new HashSet<>();
      Dispositivos d = new Dispositivos();
      d.setToken(dispositivo);
      d.setCliente(c);
      dispo.add(d);
      c.setDispositivos(dispo);
      LOG.info(c.getDispositivos().size());
      LOG.info(c.toString());
      clientesService.addClientes(c);

      return new ResponseEntity<>(c, HttpStatus.OK);

    } else {
      //si existe devuelvo error
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

  }
}
