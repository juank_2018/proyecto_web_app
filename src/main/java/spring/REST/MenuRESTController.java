/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.REST;

import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import spring.model.Menu;
import spring.utils.MenuREST;
import spring.model.Menu_has_platos;
import spring.service.MenuService;
import spring.service.Menu_has_platosSerice;

/**
 * Gestiona las peticiones REST desde la app para el menu
 *
 * @author jcpm0
 */
@RestController
public class MenuRESTController {

  private static final Logger LOG = Logger.getLogger(MenuRESTController.class);
  @Autowired
  private MenuService menuService;
  @Autowired
  private Menu_has_platosSerice mhpService;

  /**
   * Devuelve la lista de menu
   *
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/menu", method = RequestMethod.GET)
  public ResponseEntity<List<Menu>> listMenu() {
    //obtiene lista de menus
    List<Menu> phc = menuService.listMenus();
    LOG.info("entra. " + phc.toString());
    if (phc.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    return new ResponseEntity<>(phc, HttpStatus.OK);
  }

  /**
   * Devuelve un menu por el id
   *
   * @param id id del menu
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/menu/{id}", method = RequestMethod.GET)
  public ResponseEntity<List<Menu_has_platos>> listMenu(@PathVariable("id") int id) {
    List<Menu_has_platos> mhp = mhpService.listMenus_has_platos(id);
    LOG.info("entra. " + mhp.toString());
    if (mhp.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
    }
    return new ResponseEntity<>(mhp, HttpStatus.OK);
  }

  /**
   * Devuelve un menu por la fecha
   *
   * @param fecha String con la fecha
   * @return
   */
  @RequestMapping(value = "/REST/menuPorFecha/{fecha}", method = RequestMethod.GET)
  public ResponseEntity<List<MenuREST>> listMenuDate(@PathVariable("fecha") String fecha) {
    //LOG.info(results.toString());
    LOG.info(fecha);
    //creo una lista de objetos MenuREST
    List<MenuREST> resultado = new ArrayList<>();
    //lista con los tipos de plato en el menu
    List<String> tipos = new ArrayList<>();
    tipos.add("Primero");
    tipos.add("Segundo");
    tipos.add("Postre");
    //Obtengo la lista de platos del menu de esa fecha
    List<Menu_has_platos> mhp = mhpService.getMenu_has_platosByDate(fecha);
    if (mhp.isEmpty()) {
      //si está vacio, aún no ha salido el menu del día
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      //Para cada tipo de plato de menu creo un objeto MenuREST en el que almacenar
      //los platos de ese tipo y luego los mento en lista de objetos MenuREST
      for (String tipo : tipos) {
        MenuREST seccionMenu = new MenuREST();
        seccionMenu.setTipo(tipo);
        for (Menu_has_platos m : mhp) {
          if (m.getTipo().equals(tipo)) {
            seccionMenu.getPlatos().add(m.getPlato().getNombre());
          }
        }
        resultado.add(seccionMenu);
      }
      //devuelvo la lista de objetos MenuREST y 200
      return new ResponseEntity<>(resultado, HttpStatus.OK);
    }
  }
}
