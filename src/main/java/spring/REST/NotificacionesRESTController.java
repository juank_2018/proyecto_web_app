/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.REST;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import spring.model.Notificaciones;
import spring.service.NotificacionesService;

/**
 * Gestiona las peticiones REST para las notificaciones
 *
 * @author jcpm0
 */
@RestController
public class NotificacionesRESTController {

  @Autowired
  private NotificacionesService notificacionesService;

  /**
   * Devuelve la lista de las notificaciones
   *
   * @param email String con el email del cliente
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/notificaciones", method = RequestMethod.GET)
  public ResponseEntity<List<Notificaciones>> listaNotifiaciones(@RequestParam("email") String email) {
    //obtiene la lista de las notificaciones
    List<Notificaciones> lst = notificacionesService.listNotificaciones();
    //creo una lista para almacenar las que estén marcadas como entregadas
    List<Notificaciones> entregadas = new ArrayList<>();
    //relleno la lista con las entregadas
    for (Notificaciones n : lst) {
      if (n.getEntregada() == 1) {
        if (!n.getDispositivo().isEmpty()) {
          if (n.getDispositivo().get(0).getCliente().getEmail().equalsIgnoreCase(email)) {
            entregadas.add(n);
          }
        } else {
          entregadas.add(n);
        }
      }
    }
    //si la lista está vacia no ha notificaciones, devuelvo 404
    if (lst.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      //si no está vacia la devuelvo con 200
    } else {
      return new ResponseEntity<>(entregadas, HttpStatus.OK);
    }
  }
}
