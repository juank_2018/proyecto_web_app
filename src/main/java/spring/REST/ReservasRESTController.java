/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.REST;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import spring.model.Clientes;
import spring.model.Reservas;
import spring.utils.SimpleResponse;
import spring.service.ClientesService;
import spring.service.ReservasService;

/**
 * Gestiona las peticiones REST para las reservas
 *
 * @author jcpm0
 */
@RestController
public class ReservasRESTController {

  private final Logger LOG = Logger.getLogger(ReservasRESTController.class);

  @Autowired
  private ReservasService reservasService;
  @Autowired
  private ClientesService clientesService;

  /**
   * Devuelve la lista de reservas de un cliente
   *
   * @param email String con el correo del cliente
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/reservas", method = RequestMethod.GET)
  public ResponseEntity<List<Reservas>> listReservas(@RequestParam("email") String email) {
    LOG.info(email);
    //Formateo el correo con comillas simples  
    String email_limpio;
    if (email.startsWith("\"")) {
      email_limpio = email.replace("\"", "\'");
    } else {
      email_limpio = "\'" + email + "\'";
    }
    //obtiene la lista de reservas para el email dado   
    List<Reservas> mhp = reservasService.listReservasByMail(email_limpio);
    LOG.info("entra. " + mhp.toString());
    //si la lista está vacia, devuelve 404
    if (mhp.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);//You many decide to return HttpStatus.NOT_FOUND
    }
    // si no devuelve la lista y 200
    return new ResponseEntity<>(mhp, HttpStatus.OK);
  }

  /**
   * Gestiona la solicitud de una reserva
   *
   * @param idCliente int con el id del cliente
   * @param turno String con el turno
   * @param comensales int con el número de comensales
   * @param fechaHora String con la fecha y la hora de llegada
   * @return Objeto ResponseEntity
   */
  @RequestMapping(value = "/REST/solicitaReserva", method = RequestMethod.POST)
  public ResponseEntity<SimpleResponse> solicitaReserva(@RequestParam("id") int idCliente, @RequestParam("turno") String turno,
          @RequestParam("nComensales") int comensales, @RequestParam("fechaHora") String fechaHora) {

    try {
      //busca al cliente
      Clientes c = clientesService.getClientesById(idCliente);
      //instancio un nuevo objeto reservas
      Reservas r = new Reservas();
      LOG.info(c.toString());
      LOG.info(fechaHora);
      //formateo la fecha
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
      Date fecha = sdf.parse(fechaHora);
      LOG.info(fecha.toString());
      //preparo el objeto para insertar
      r.setCliente(c);
      r.setTurno(turno);
      r.setnComensales(comensales);
      r.setFechaHora(fecha);
      //lo inserto
      reservasService.addReservas(r);
      //preparo una respuesta
      SimpleResponse respuest = new SimpleResponse();
      respuest.setError(false);
      respuest.setMessage("Reserva solicitada");
      //devuelvo la respuesta y 200
      return new ResponseEntity<>(respuest, HttpStatus.OK);
    } catch (ParseException e) {
      LOG.info(e.getMessage());
      //si hay algún error
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
  }
}
