/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

/**
 * Con esta clase se redirige al usuario a la pagina de inicio si se autentifica
 * correctamente
 *
 * @author jcpm0
 */
@Component
public class CustomizeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

  private Logger logger = Logger.getLogger(this.getClass());

  /**
   *
   * @param request
   * @param response
   * @param authentication
   * @throws IOException
   * @throws ServletException
   */
  @Override
  public void onAuthenticationSuccess(HttpServletRequest request,
          HttpServletResponse response, Authentication authentication)
          throws IOException, ServletException {
    //set our response to OK status
    response.setStatus(HttpServletResponse.SC_OK);

    boolean admin = false;

    logger.info("AT onAuthenticationSuccess(...) function!");
    response.sendRedirect("/spring/inicio");
  }
}
