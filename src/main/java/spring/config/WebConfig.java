/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.format.FormatterRegistry;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import org.springframework.web.servlet.view.InternalResourceViewResolver;
import spring.utils.DateStringConverter;
import spring.utils.StringDateConverter;

/**
 * Clase de configuración de Spring. Se utilizan anotaciones para indicar a
 * Spring que configurar.
 *
 * @author jcpm0
 */
@EnableWebSecurity
@Configuration//marca la clase como de configuración
@EnableWebMvc//Indica a spring que use el módulo MVC
@PropertySource({"classpath:db.properties",
  "classpath:mail.properties"})//archivos con propiedades
@EnableTransactionManagement//Indica a Spring que se encargue de gestionar las transacciones 
@ComponentScan("spring")//Indica donde tiene que buscar las clases marcadas como componentes
public class WebConfig implements WebMvcConfigurer {

  /**
   *
   * @param registry
   */
  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/login").setViewName("login");
    registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
  }

  /**
   * Método para definir el viewResolver, es el Bean que se encarga de resolver
   * la vista a la que hace referencia un controlador. En el método se configura
   * el directorio dentro del proyecto donde van a estar los archivos de la
   * vista y el sufijo que van a tener.
   *
   * @return devuelve un Bean viewResolver configurado
   */
  @Bean
  public ViewResolver viewResolver() {
    InternalResourceViewResolver resolver
            = new InternalResourceViewResolver();
    resolver.setPrefix("/WEB-INF/jsp/");
    resolver.setSuffix(".jsp");
    resolver.setExposeContextBeansAsAttributes(true);
    return resolver;
  }

  /**
   * Método para añadir un manejador de recursos. Es útil para poder hacer
   * referencia a recursos estáticos tipo css o js desde las vistas. Se le
   * indica a Spring en que directorio del proyecto estan
   *
   * @param registry
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/css/**")
            .addResourceLocations("/resources/css")
            .setCachePeriod(31556926);
    registry.addResourceHandler("/js/**")
            .addResourceLocations("/resources/js")
            .setCachePeriod(31556926);
    registry.addResourceHandler("**")
            .addResourceLocations("/resources")
            .setCachePeriod(31556926);
  }

  /**
   *
   * @param configurer
   */
  @Override
  public void configureDefaultServletHandling(
          DefaultServletHandlerConfigurer configurer) {
    configurer.enable();
  }

  /**
   * Método para registrar formateadores para convertir de Date a String y
   * viceversa
   *
   * @param registry
   */
  @Override
  public void addFormatters(FormatterRegistry registry) {
    registry.addConverter(new StringDateConverter());
    registry.addConverter(new DateStringConverter());
  }
  @Autowired
  private Environment env;

  /**
   * Método para configura un DataSource, es el Bean a través del cual Spring va
   * a realizar las conexiones a la base de datos. Toma los datos de
   * configuración del fichero db.properties
   *
   * @return devuelve un DataSource configurado.
   */
  @Bean
  public DataSource getDataSource() {
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName(env.getProperty("db.driver"));
    dataSource.setUrl(env.getProperty("db.url"));
    dataSource.setUsername(env.getProperty("db.username"));
    dataSource.setPassword(env.getProperty("db.password"));

    return dataSource;
  }

  /**
   * Método para crear un Bean para que Spring gestione las sesiones de
   * Hibernate
   *
   * @return
   */
  @Bean
  public LocalSessionFactoryBean sessionFactory() {
    LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
    sessionFactory.setDataSource(getDataSource());

    Properties props = new Properties();
    props.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
    props.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
    props.put("hibernate.enable_lazy_load_no_trans",
            env.getProperty("hibernate.enable_lazy_load_no_trans"));
    sessionFactory.setHibernateProperties(props);

    sessionFactory.setPackagesToScan("spring.model");
    return sessionFactory;
  }

  /**
   * Bean para manejar las transacciones desde hibernate a la BBDD
   *
   * @return
   */
  @Bean
  public HibernateTransactionManager getTransactionManager() {
    HibernateTransactionManager transactionManager
            = new HibernateTransactionManager();
    transactionManager.setSessionFactory(sessionFactory().getObject());
    return transactionManager;
  }

  /**
   * Método para configurar un Bean para poder enviar correos electrónicos desde
   * Spring.
   *
   * @return
   */
  @Bean
  public JavaMailSenderImpl mailSender() {
    JavaMailSenderImpl jm = new JavaMailSenderImpl();
    jm.setHost("smtp.gmail.com");
    jm.setUsername("pruebas.proyectoDAM@gmail.com");
    jm.setPassword("Pituitaria1");
    jm.setPort(587);
    Properties props = new Properties();
    props.put("mail.transport.protocol", env.getProperty("mail.transport.protocol"));
    props.put("mail.smtp.auth", env.getProperty("mail.smtp.auth"));
    props.put("mail.smtp.starttls.enable", env.getProperty("mail.smtp.starttls.enable"));
    props.put("mail.debug", env.getProperty("mail.debug"));
    props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
    jm.setJavaMailProperties(props);

    return jm;
  }
}
