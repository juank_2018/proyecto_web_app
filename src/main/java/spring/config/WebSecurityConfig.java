/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.config;

import javax.sql.DataSource;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Clase de configuración de Spring Security
 *
 * @author jcpm0
 */
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

  private static final Logger LOG
          = Logger.getLogger(WebSecurityConfig.class);
  private static String REALM = "MY_TEST_REALM";
  @Autowired
  private DataSource dataSource;
  @Autowired
  CustomizeLogoutSuccessHandler customizeLogoutSuccessHandler;
  @Autowired
  CustomizeAuthenticationSuccessHandler customizeAuthenticationSuccessHandler;

  /**
   *
   * @param auth
   * @throws Exception
   */
  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    // ensure the passwords are encoded properly
    LOG.info("entra");
    auth
            .jdbcAuthentication()
            .dataSource(dataSource)
            .usersByUsernameQuery("SELECT login,paswd,true FROM empleados WHERE login = ?")
            .authoritiesByUsernameQuery("SELECT login,rol FROM empleados WHERE login = ?")
            .passwordEncoder(new BCryptPasswordEncoder());

//  LOG.info(dataSource.getConnection("root","").prepareStatement("SELECT login,rol FROM empleados WHERE login = ?").executeQuery().getString(1));
    LOG.info(auth.jdbcAuthentication().authoritiesByUsernameQuery("SELECT login,rol FROM empleados WHERE login = ?").toString());
  }

  /**
   *
   * @param web
   * @throws Exception
   */
  @Override
  public void configure(WebSecurity web) throws Exception {
    web.debug(true);
  }

  /**
   *
   * @param http
   * @throws Exception
   */
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
            .authorizeRequests()
            .antMatchers("/login").permitAll()
            .antMatchers("/REST/**").permitAll()
            .antMatchers("/resources/**").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin().successHandler(customizeAuthenticationSuccessHandler)
            .loginPage("/login")
            .permitAll()
            .and()
            .logout()
            .logoutSuccessHandler(customizeLogoutSuccessHandler)
            .permitAll()
            .invalidateHttpSession(true);

  }
}
