/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.controller;

import java.util.List;
import org.jboss.logging.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spring.model.Empleados;
import spring.model.Usuarios;
import spring.service.EmpleadosService;
import spring.service.UsuariosService;

/**
 * Controlador para gestionar los empleados
 * @author jcpm0
 */
@Controller
public class EmpleadosController {
   private static final Logger LOG
          = Logger.getLogger(EmpleadosController.class.getName());
  @Autowired
  EmpleadosService empleadosService;
  @Autowired
  UsuariosService usuariosService;
  
  
  /**
   * Método para listar los empleados
   * @param model Objeto model
   * @param offset Desplazamiento sobre el total de registros
   * @param maxResults Máximo resultado de la consulta
   * @return  nombre de la vista
   */
    @RequestMapping(value = "empleados")
  public String list(Model model, Integer offset, Integer maxResults) {
    if (offset == null) {
      offset = 0;
    }

    /* cuento los platos que tiene la carta.*/
    int count = empleadosService.listEmpleados().size();
    /*Añado al modelo la lista de platos de la carta*/
    model.addAttribute("empleados",
            empleadosService.listarEmpleados(offset, maxResults));
    /*Añado al modelo el número total de platos*/
    model.addAttribute("count", count);
    /*Añado al modelo el offset*/
    model.addAttribute("offset", offset);

    return "empleados";
  }
  /**
   * Método para eliminar un empleado
   * Si sólo queda un empleado en la base de datos no permite la operación
   * y pasa error a la vista
   * @param id el id del empleado a borrar
   * @param model Objeto Model
   * @return  nombre de la vista
   */
  @RequestMapping(value="/borraEmpleado/{id}", method = RequestMethod.GET)
  public String borraEmpleado(@PathVariable int id,Model model){
    
    /*No se pueden borrar todos los empleados, así que si el tamaño de la lista
    de empleados es 1 no continúo*/
    List<Empleados> lista = empleadosService.listEmpleados();
    
    if(lista.size() == 1){
      model.addAttribute("msg", "No se pueden borrar todos los empleados");
      
    }else{
      empleadosService.removeEmpleados(id);
     
    }
     model.addAttribute("empleados", empleadosService.listarEmpleados(0, 5));
    return "empleados";
  }
  
  @RequestMapping(value="/crearEmpleado", method = RequestMethod.GET)
  public String crearPlato(Model model){
    
    Empleados empleado = new Empleados();
    Usuarios usuario = new Usuarios();
    empleado.setUsuario(usuario);
    
    model.addAttribute("empleado", empleado);
    
    return "creaEmpleado";
  }
  
  @RequestMapping(value="creaEmpleado",method = RequestMethod.POST)
  public String insertaEmpleado(Model model, @ModelAttribute ("empleado")Empleados empleado){
    
    LOG.info(empleado.getLogin());
    LOG.info(empleado.getUsuario().getNombre());
    LOG.info(empleado.getUsuario().getFechanacimiento());
    LOG.info(empleado.getRol());
    usuariosService.addUsuarios(empleado.getUsuario());
    
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    String paswdEncriptada = encoder.encode(empleado.getPaswd());
    empleado.setPaswd(paswdEncriptada);
    
    empleadosService.addEmpleados(empleado);
    
    model.addAttribute("empleados", empleadosService.listarEmpleados(0, 10));
    
    return "empleados";
  }
}
