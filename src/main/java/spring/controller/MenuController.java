/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spring.model.Menu;
import spring.model.Menu_has_platos;
import spring.model.Platos;
import spring.service.MenuService;
import spring.service.Menu_has_platosSerice;
import spring.service.PlatosService;
import spring.utils.MenuDetalle;
import spring.utils.MenuDetalle.Plato;

/**
 * Controlador para las operaciones con los menús
 *
 * @author jcpm0
 */
@Controller
public class MenuController {

  private static final Logger LOG
          = Logger.getLogger(MenuController.class.getName());

  @Autowired
  private MenuService menuService;
  @Autowired
  private Menu_has_platosSerice mhpService;
  @Autowired
  private PlatosService platosService;

  /**
   * Método para listar los menús.
   *
   * @param model objeto Model para pasar datos a la vista
   * @param offset Desplazamiento sobre el total de menús en la lista de menus
   * @param maxResults Resultados máximos por página
   * @return
   */
  @RequestMapping(value = "menu")
  public String list(Model model, Integer offset, Integer maxResults) {

    if (offset == null) {
      offset = 0;
    }
    Menu menu = new Menu();
    /*Contamos el número de cartas para pasarselo a la vista para el paginador*/
    int count = menuService.listMenus().size();
    /*paso a la vista la lista de cartas*/
    model.addAttribute("menus", menuService.listMenus(offset, maxResults));
    /*el número total de registros para el paginador*/
    model.addAttribute("count", count);
    /*desplazamiento dentro del número de registros.*/
    model.addAttribute("offset", offset);
    /*objeto menu*/
    model.addAttribute("menu", menu);
    /*devuelve el nombre de la vista que tiene que cargar*/
    return "menu";

  }

  /**
   * Método para crear un nuevo menu
   *
   * @param menu Objeto menu en el que va a recibir los datos del formulario
   * @return una vez se crea el menu se redirecciona a la vista menu.jsp
   */
  @RequestMapping(value = "/crearMenu", method = RequestMethod.POST)
  public String cearMenu(@ModelAttribute("menu") Menu menu) {
    menuService.addMenu(menu);
    return "redirect:menu";
  }

  /**
   *
   * @param id
   * @param menu
   * @param model
   * @param results
   * @return
   */
  @RequestMapping(value = "/borraMenu/{id}", method = RequestMethod.GET)
  public String borrarMenu(@PathVariable int id, @ModelAttribute("menu") Menu menu, Model model, BindingResult results) {
    LOG.info(results.toString());
    menuService.removeMenu(id);
    int count = menuService.listMenus().size();
    /*paso a la vista la lista de menus*/
    model.addAttribute("menus", menuService.listMenus(0, 5));
    /*el número total de registros para el paginador*/
    model.addAttribute("count", count);
    /*desplazamiento dentro del número de registros.*/
    model.addAttribute("offset", 0);
    /*objeto menu*/
    model.addAttribute("menu", menu);
    /*devuelve el nombre de la vista que tiene que cargar*/
    return "menu";
  }

  /**
   * Método para editar un menu
   *
   * @param model Objeto Model con los datos para la vista
   * @param detalle Objeto MenuDetalle para cargar los detalles del menu
   * @param id Id del menú que se va a editar
   * @return
   */
  @RequestMapping(value = "/editaMenu/{id}", method = RequestMethod.GET)
  public String editaMenu(Model model, @ModelAttribute("detalle") MenuDetalle detalle,
          @PathVariable int id) {

    /*obtengo el menu*/
    Menu menu = menuService.getMenuById(id);

    /*Preparo el objeto MenuDetalle*/
    detalle = new MenuDetalle();
    detalle.setMenu(menu);
    detalle = rellenaDetalle(mhpService.listMenus_has_platos(menu.getIdmenu()), detalle, menu);
    /*preparo datos para la vista*/
    LOG.info(detalle.getPrimeros().size() + " " + detalle.getSegundos().size() + " " + detalle.getPostres().size());
    model.addAttribute("plato", platosService.listarPlatosMenu());
    model.addAttribute("detalle", detalle);
    model.addAttribute("menus", menuService.listMenus());

    model.addAttribute("menu", new Menu());
    return "editaMenu";
  }

  /**
   * Método para listar los platos de un menu concreto
   *
   * @param model Datos para la vista.
   * @param id id del menu que se quiere consultar
   * @param detalle Objeto MenuDetalle con los detalles del menu
   * @return
   */
  @RequestMapping(value = "/listadoMenu/{id}", method = RequestMethod.GET)
  public String listadoMenu(Model model,
          @PathVariable int id, @ModelAttribute("detalle") MenuDetalle detalle) {
    /*Obtengo el menu de la bbdd*/
    Menu menu = menuService.getMenuById(id);
    /*Obtengo los platos que contiene el menu y los paso a la vista*/
    model.addAttribute("mhp", mhpService.listMenus_has_platos(menu.getIdmenu()));
    /*También le paso el menu */
    model.addAttribute("menu", menu);
    return "listaPlatosMenu";
  }

  /**
   * Método para copiar un menú anterior
   *
   * @param model
   * @param detalle
   * @return
   */
  @RequestMapping(value = "/copiaMenu", method = RequestMethod.POST)
  public String copiaMenu(Model model, @ModelAttribute MenuDetalle detalle) {

    if (detalle == null) {
      LOG.info(detalle);
      return "editaMenu";
    } else {
      LOG.info(detalle.getMenu().getIdmenu());
      /*obtengo la fecha del menu y la formateo tal como está en la bdd*/
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      String fecha = sdf.format(detalle.getMenu().getFecha());

      /*Obtengo el menu correspondiente a esa fecha*/
      Menu m = menuService.getMenuByDate(fecha);
      Menu menuActual = menuService.getMenuById(detalle.getMenu().getIdmenu());
      /*Preparo el objeto MenuDetalle*/
      MenuDetalle detalle1 = new MenuDetalle();
      detalle1.setMenu(detalle.getMenu());
      detalle1 = rellenaDetalle(mhpService.listMenus_has_platos(m.getIdmenu()), detalle, menuActual);

      /*preparo datos para la vista*/
      model.addAttribute("plato", platosService.listarPlatosMenu());
      model.addAttribute("detalle", detalle);
      model.addAttribute("menus", menuService.listMenus());
      return "editaMenu";
    }

  }

  /**
   * Método para actualizar un menu existente
   *
   * @param model Objeto Model para pasar datos a la vista
   * @param detalle Objeto MenuDetalle con el detalle del menu
   *
   * @return
   */
  @RequestMapping(value = "/actualizaMenu", method = RequestMethod.POST)
  public String actualizaMenu(Model model, @ModelAttribute("detalle") MenuDetalle detalle) {

    Menu menu = detalle.getMenu();

    insertaPlatoMenu(detalle, menu);
    int count = menuService.listMenus().size();
    /*paso a la vista la lista de cartas*/
    model.addAttribute("menus", menuService.listMenus(0, 5));
    /*el número total de registros para el paginador*/
    model.addAttribute("count", count);
    /*desplazamiento dentro del número de registros.*/
    model.addAttribute("offset", 0);
    /*objeto carta*/
    model.addAttribute("menu", menu);

    return "menu";

  }

  /**
   * Se encarga de generar un Map con los platos del menu para pasarselos a la
   * vista y poblar el select del formulario
   *
   * @return
   */
  @ModelAttribute("platos")
  public Map<Integer, String> rellenaPlatos() {
    List<Platos> lst = platosService.listarPlatosMenu();

    Map<Integer, String> tipos = new HashMap<>();
    for (Platos p : lst) {
      int i = p.getIdPlato();
      String tipo = p.getNombre();
      tipos.put(i, tipo);
    }
    return tipos;
  }

  /**
   * Recibe una lista de platos de un menu y devuelve un objeto MenuDetalle
   * relleno con los datos
   *
   * @param lista Lista de platos de un menu
   * @param detalle Objeto MenuDetalle para rellenar
   * @param menu Menu del que se quieren rellenar los platos.
   * @return
   */
  private MenuDetalle rellenaDetalle(List<Menu_has_platos> lista,
          MenuDetalle detalle, Menu menu) {

    detalle.setMenu(menu);
    /*si la lista está vacía es que no se ha seleccionado ningún plato en el 
    formulario, entonces se rellena el primer plato de cada caregoria con
    el strin "Selecciona plato..."*/
    if (lista.isEmpty()) {
      for (int i = 0; i < 3; i++) {
        Plato p = new Plato();
        p.setPlato("Selecciona plato...");
        p.setIdplato(0);
        detalle.getPrimeros().add(p);
        detalle.getSegundos().add(p);
        detalle.getPostres().add(p);

      }
    }
    /*si no se recorre y se rellena MenuDetalle */
    for (Menu_has_platos mhp : lista) {
      switch (mhp.getTipo()) {
        case "Primero": {
          Plato p = new Plato();
          String nombre = mhp.getPlato().getNombre();
          int id = mhp.getPlato().getIdPlato();
          p.setPlato(nombre.trim());
          p.setIdplato(id);
          detalle.getPrimeros().add(p);
        }
        break;
        case "Segundo": {
          Plato p = new Plato();
          String nombre = mhp.getPlato().getNombre();
          int id = mhp.getPlato().getIdPlato();
          p.setPlato(nombre.trim());
          p.setIdplato(id);
          detalle.getSegundos().add(p);
        }
        break;
        case "Postre": {
          Plato p = new Plato();
          String nombre = mhp.getPlato().getNombre();
          int id = mhp.getPlato().getIdPlato();
          p.setPlato(nombre.trim());
          p.setIdplato(id);
          detalle.getPostres().add(p);
        }
        break;

      }
    }

    return detalle;
  }

  /**
   * Recibe un objeto MenuDetalle y devuelve una lista de objetos Menu_has_plato
   *
   * @param detalle
   * @param menu
   * @return
   */
  private List<Menu_has_platos> MenuDetalleAMenu_has_Platos(MenuDetalle detalle, Menu menu) {
    List<Menu_has_platos> datos_formulario = new ArrayList<>();

    for (Plato p : detalle.getSegundos()) {
      Menu_has_platos m = new Menu_has_platos();
      m.setMenu(menu);
      m.setTipo("Segundo");
      Platos plato = platosService.getPlatosById(Integer.parseInt(p.getPlato()));
      m.setPlato(plato);
      datos_formulario.add(m);
    }
    for (Plato p : detalle.getPrimeros()) {
      Menu_has_platos m = new Menu_has_platos();
      m.setMenu(menu);
      m.setTipo("Primero");
      Platos plato = platosService.getPlatosById(Integer.parseInt(p.getPlato()));
      m.setPlato(plato);
      datos_formulario.add(m);
    }

    for (Plato p : detalle.getPostres()) {
      Menu_has_platos m = new Menu_has_platos();
      m.setMenu(menu);
      m.setTipo("Postre");
      Platos plato = platosService.getPlatosById(Integer.parseInt(p.getPlato()));
      m.setPlato(plato);
      datos_formulario.add(m);
    }
    return datos_formulario;
  }

  /**
   * Este método recibe como parámetros un objeto MenuDetalle con los detallse
   * de un menú y un Objeto Menú. Si el menú no tiene platos asociados en la
   * BBDD el método asocia los platos de MenuDetalle con el menú en la BBDD, si
   * el menú ya tiene platos asociados el método actualiza la BBDD con los que
   * recibe en MenuDetalle.
   *
   * @param detalle
   * @param menu
   */
  private void insertaPlatoMenu(MenuDetalle detalle, Menu menu) {

    /*Obtiene la lista de platos del menu*/
    LOG.info("menu" + menu.getIdmenu());
    List<Menu_has_platos> lst = mhpService.listMenus_has_platos(menu.getIdmenu());
    LOG.info("lst " + lst.toString());
    List<Menu_has_platos> datos_formulario;
    Menu menu1;
    menu1 = menuService.getMenuById(menu.getIdmenu());
    LOG.info("menu1 " + menu1.toString());
    if (lst.isEmpty()) {
      LOG.info("if");
      //menuService.updateMenu(menu);
      /*si esta lista está vacia es que el menú aún no tiene platos*/
 /*Paso los datos del formulario a una lista de objetos Menu_has_platos*/
      datos_formulario = MenuDetalleAMenu_has_Platos(detalle, menu1);
      LOG.info("datos " + datos_formulario.toString());
      /*los inserto en la BBDD*/
      for (Menu_has_platos m : datos_formulario) {

        mhpService.addMenu_has_platos(m);
      }
    } else {
      LOG.info("else");
      /*si la lista tiene items es que es un menu que se está editando*/
 /*Paso los datos del formulario a una lista de objetos Menu_has_platos*/
      datos_formulario = MenuDetalleAMenu_has_Platos(detalle, menu);
      for (Menu_has_platos m : datos_formulario) {
        LOG.info("datos_formulario: " + m.getTipo() + " " + m.getPlato().getNombre());
      }
      for (Menu_has_platos m : lst) {
        LOG.info("lst :" + m.getTipo() + " " + m.getPlato().getNombre());
      }
      /*Recorro la lista de platos del menu que esta en la BBDD*/
      for (int i = 0; i < lst.size(); i++) {
        mhpService.removeMenu_has_platos(lst.get(i).getPrimaryKey());
      }
      for (Menu_has_platos m : datos_formulario) {
        mhpService.addMenu_has_platos(m);
      }
    }

  }

}
