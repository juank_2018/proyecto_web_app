/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.controller;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import spring.model.Reservas;
import spring.service.ReservasService;

/**
 *
 * @author jcpm0
 */
@Controller
public class inicioController {

  private static final Logger LOG
          = Logger.getLogger(inicioController.class.getName());

  /**
   *
   */
  @Autowired
  public ReservasService reservasService;

  /**
   *
   * @param model
   * @param offset
   * @param maxResults
   * @return
   */
  @RequestMapping(value = "inicio", method = RequestMethod.GET)
  public String list(Model model, Integer offset, Integer maxResults) {
    LOG.info("entra en handledrequest");
    if (offset == null) {
      offset = 0;
    }

    List<Reservas> lst = reservasService.listarReservas(offset, maxResults);
    for (Reservas r : lst) {
      LOG.info(r.toString());
    }
    Date fechaAcutal = new Date();
    Reservas reserva = new Reservas();
    /*Contamos el número de cartas para pasarselo a la vista para el paginador*/
    int count = reservasService.listReservas().size();
    /*paso a la vista la lista de cartas*/
    model.addAttribute("reservas", lst);
    /*el número total de registros para el paginador*/
    model.addAttribute("count", count);
    /*desplazamiento dentro del número de registros.*/
    model.addAttribute("offset", offset);
    /*objeto carta*/
    model.addAttribute("reserva", reserva);
    model.addAttribute("fechaActual", fechaAcutal);
    return "inicio";
  }

  /**
   *
   * @param id
   * @return
   */
  @RequestMapping(value = "/borraReserva/{id}", method = RequestMethod.GET)
  public ModelAndView delete(@PathVariable int id) {
    /*Llamada al servicio para borrar la reserva*/
    reservasService.removeReservas(id);
    /*volvemos a la vista reservas*/
    return new ModelAndView("redirect:/inicio");
  }

}
