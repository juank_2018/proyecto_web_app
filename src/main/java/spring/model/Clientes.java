/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author jcpm0
 */
@Entity
@Table(name = "clientes")
public class Clientes implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idclientes")
  private int idclientes;

  @Column(name = "email")
  private String email;

  @Column(name = "paswd")
  private String paswd;

  @OneToMany(mappedBy = "cliente", cascade = {CascadeType.REFRESH, CascadeType.MERGE})
  private Set<Dispositivos> dispositivos;

  @JsonIgnore
  @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
  private Set<Reservas> reservas;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "usuarios_idusuarios")
  private Usuarios usuarios;

  /**
   *
   */
  public Clientes() {
  }

  /**
   *
   * @param email
   * @param paswd
   * @param dispositivos
   * @param reservas
   * @param usuario
   */
  public Clientes(String email, String paswd, Set<Dispositivos> dispositivos, Set<Reservas> reservas, Usuarios usuario) {
    this.email = email;
    this.paswd = paswd;
    this.dispositivos = dispositivos;
    this.reservas = reservas;
    this.usuarios = usuario;
  }

  /**
   *
   * @return
   */
  public Set<Reservas> getReservas() {
    return reservas;
  }

  /**
   *
   * @param reservas
   */
  public void setReservas(Set<Reservas> reservas) {
    this.reservas = reservas;
  }

  /**
   *
   * @return
   */
  public long getIdclientes() {
    return idclientes;
  }

  /**
   *
   * @param idclientes
   */
  public void setIdclientes(int idclientes) {
    this.idclientes = idclientes;
  }

  /**
   *
   * @return
   */
  public String getEmail() {
    return email;
  }

  /**
   *
   * @param email
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   *
   * @return
   */
  public String getPaswd() {
    return paswd;
  }

  /**
   *
   * @param paswd
   */
  public void setPaswd(String paswd) {
    this.paswd = paswd;
  }

  /**
   *
   * @return
   */
  public Usuarios getUsuario() {
    return usuarios;
  }

  /**
   *
   * @param usuario
   */
  public void setUsuario(Usuarios usuario) {
    this.usuarios = usuario;
  }

  /**
   *
   * @return
   */
  public Set<Dispositivos> getDispositivos() {
    return dispositivos;
  }

  /**
   *
   * @param dispositivos
   */
  public void setDispositivos(Set<Dispositivos> dispositivos) {
    this.dispositivos = dispositivos;
  }

  @Override
  public String toString() {
    return "Clientes{" + "idclientes=" + idclientes + ", email=" + email + ", paswd=" + paswd + ", usuario=" + usuarios + '}';
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 11 * hash + this.idclientes;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Clientes other = (Clientes) obj;
    if (this.idclientes != other.idclientes) {
      return false;
    }
    return true;
  }

}
