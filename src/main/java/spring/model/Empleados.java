/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import javax.validation.constraints.NotEmpty;

/**
 *
 * @author jcpm0
 */
@Entity
@Table(name = "empleados")
public class Empleados implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idempleados")
  private int idempleados;
  @Column(name = "login")
  private String login;
  @Column(name = "paswd")
  private String paswd;
  @Column(name = "rol")
  private String rol;
  @ManyToOne
  @JoinColumn(name = "usuarios_idusuarios")
  @JsonIgnore
  private Usuarios usuarios;
  @JsonIgnore
  @OneToMany(mappedBy = "empleados", cascade = {CascadeType.ALL})
  private Set<Platos> platos;

  /**
   *
   */
  public Empleados() {
  }

  /**
   *
   * @param login
   * @param paswd
   * @param rol
   * @param usuario
   * @param platos
   */
  public Empleados(String login, String paswd, String rol, Usuarios usuario, Set<Platos> platos) {
    this.login = login;
    this.paswd = paswd;
    this.rol = rol;
    this.usuarios = usuario;
    this.platos = platos;
  }

  /**
   *
   * @return
   */
  public Set<Platos> getPlatos() {
    return platos;
  }

  /**
   *
   * @param platos
   */
  public void setPlatos(Set<Platos> platos) {
    this.platos = platos;
  }

  /**
   *
   * @return
   */
  public long getIdempleados() {
    return idempleados;
  }

  /**
   *
   * @param idempleados
   */
  public void setIdempleados(int idempleados) {
    this.idempleados = idempleados;
  }

  /**
   *
   * @return
   */
  public String getLogin() {
    return login;
  }

  /**
   *
   * @param login
   */
  public void setLogin(String login) {
    this.login = login;
  }

  /**
   *
   * @return
   */
  public String getPaswd() {
    return paswd;
  }

  /**
   *
   * @param paswd
   */
  public void setPaswd(String paswd) {
    this.paswd = paswd;
  }

  /**
   *
   * @return
   */
  public String getRol() {
    return rol;
  }

  /**
   *
   * @param rol
   */
  public void setRol(String rol) {
    this.rol = rol;
  }

  /**
   *
   * @return
   */
  public Usuarios getUsuario() {
    return usuarios;
  }

  /**
   *
   * @param usuario
   */
  public void setUsuario(Usuarios usuario) {
    this.usuarios = usuario;
  }

  @Override
  public String toString() {
    return "Empleados{" + "idempleados=" + idempleados + ", login=" + login + ", paswd=" + paswd + ", rol=" + rol + ", usuario=" + usuarios + '}';
  }

}
