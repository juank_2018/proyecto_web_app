/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import spring.utils.JsonDateSerializer;

/**
 *
 * @author jcpm0
 */
@Entity
@Table(name = "notificaciones")
public class Notificaciones implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idnotificaciones")
  private int idnotificaciones;

  @Column(name = "mensaje")
  private String mensaje;

  @JsonSerialize(using = JsonDateSerializer.class)
  @Column(name = "fecha")
  @Temporal(TemporalType.DATE)
  private Date fecha;

  @ManyToMany(cascade =  {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.DETACH})
   @JoinTable(name = "dispositivos_has_notificaciones", joinColumns = {
    @JoinColumn(name = "notificaciones_idnotificaciones")}, inverseJoinColumns = {
    @JoinColumn(name = "dispositivos_iddispositivos")})
  private List<Dispositivos> dispositivo = new ArrayList<>();
  @JsonIgnore
  @OneToMany(mappedBy = "notificacion", cascade = CascadeType.ALL)
  private Set<Notif_sistema> notif_sistema = new HashSet<>();

  @Column(name = "entregada")
  private int entregada;

  /**
   *
   */
  public Notificaciones() {
  }

  /**
   *
   * @param mensaje
   * @param fecha
   * @param notif_sistema
   * @param entregada
   */
  public Notificaciones(String mensaje, Date fecha, Set<Notif_sistema> notif_sistema, int entregada) {
    this.mensaje = mensaje;
    this.fecha = fecha;
    this.notif_sistema = notif_sistema;
    this.entregada = entregada;
  }

//  @Override
//  public String toString() {
//    return "Notificaciones{" + "idnotificaciones=" + idnotificaciones + ", mensaje=" + mensaje + ", fecha=" + fecha + ", dispositivo=" + dispositivo + ", notif_sistema=" + notif_sistema + ", entregada=" + entregada + '}';
//  }
  /**
   *
   * @return
   */
  public int getEntregada() {
    return entregada;
  }

  /**
   *
   * @param entregada
   */
  public void setEntregada(int entregada) {
    this.entregada = entregada;
  }

  /**
   *
   * @return
   */
  public int getIdnotificaciones() {
    return idnotificaciones;
  }

  /**
   *
   * @param idnorificaciones
   */
  public void setIdnotificaciones(int idnorificaciones) {
    this.idnotificaciones = idnorificaciones;
  }

  /**
   *
   * @return
   */
  public String getMensaje() {
    return mensaje;
  }

  /**
   *
   * @param mensaje
   */
  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }

  /**
   *
   * @return
   */
  public Date getFecha() {
    return fecha;
  }

  /**
   *
   * @param fecha
   */
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  /**
   *
   * @return
   */
  public List<Dispositivos> getDispositivo() {
    return dispositivo;
  }

  /**
   *
   * @param dispositivo
   */
  public void setDispositivo(List<Dispositivos> dispositivo) {
    this.dispositivo = dispositivo;
  }

  /**
   *
   * @return
   */
  public Set<Notif_sistema> getNotif_sistema() {
    return notif_sistema;
  }

  /**
   *
   * @param notif_sistema
   */
  public void setNotif_sistema(Set<Notif_sistema> notif_sistema) {
    this.notif_sistema = notif_sistema;
  }

}
