/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import spring.utils.JsonDateSerializer;

/**
 *
 * @author jcpm0
 */
@Entity
@Table(name = "reservas")
public class Reservas implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idreservas")
  private int idreservas;

  @Column(name = "nComensales")
  private int nComensales;

  @Column(name = "turno")
  private String turno;

  @JsonSerialize(using = JsonDateSerializer.class)
  @Column(name = "fechaHora")
  @Temporal(TemporalType.TIMESTAMP)
  private Date fechaHora;

  @ManyToOne
  @JoinColumn(name = "clientes_idclientes")
  private Clientes cliente;

  /**
   *
   */
  public Reservas() {
  }

  /**
   *
   * @param nComensales
   * @param turno
   * @param fechaHora
   * @param cliente
   */
  public Reservas(int nComensales, String turno,
          Date fechaHora, Clientes cliente) {
    this.nComensales = nComensales;
    this.turno = turno;

    this.fechaHora = fechaHora;
    this.cliente = cliente;
  }

  /**
   *
   * @return
   */
  public int getIdreservas() {
    return idreservas;
  }

  /**
   *
   * @param idreservas
   */
  public void setIdreservas(int idreservas) {
    this.idreservas = idreservas;
  }

  /**
   *
   * @return
   */
  public int getnComensales() {
    return nComensales;
  }

  /**
   *
   * @param nComensales
   */
  public void setnComensales(int nComensales) {
    this.nComensales = nComensales;
  }

  /**
   *
   * @return
   */
  public String getTurno() {
    return turno;
  }

  /**
   *
   * @param turno
   */
  public void setTurno(String turno) {
    this.turno = turno;
  }

  /**
   *
   * @return
   */
  public Date getFechaHora() {
    return fechaHora;
  }

  /**
   *
   * @param fecha
   */
  public void setFechaHora(Date fecha) {
    this.fechaHora = fecha;
  }

  /**
   *
   * @return
   */
  public Clientes getCliente() {
    return cliente;
  }

  /**
   *
   * @param cliente
   */
  public void setCliente(Clientes cliente) {
    this.cliente = cliente;
  }

  @Override
  public String toString() {
    return "Reservas{" + "idreservas=" + idreservas + ", nComensales="
            + nComensales + ", turno=" + turno + ", fecha="
            + fechaHora + ", cliente=" + cliente + '}';
  }

}
