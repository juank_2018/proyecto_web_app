/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author jcpm0
 */
@Entity
@Table(name = "tipo_plato")
public class Tipo_plato implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idTipo")
  private int idTipo;

  @Column(name = "tipo")
  private String tipo;
  @JsonIgnore
  @ManyToMany(cascade = {CascadeType.ALL}, mappedBy = "tipo_plato")
  private Set<Platos> plato = new HashSet();

  /**
   *
   * @param tipo
   */
  public Tipo_plato(String tipo) {
    this.tipo = tipo;
  }

  /**
   *
   */
  public Tipo_plato() {
  }

  /**
   *
   * @return
   */
  public int getIdTipo() {
    return idTipo;
  }

  /**
   *
   * @param idTipo
   */
  public void setIdTipo(int idTipo) {
    this.idTipo = idTipo;
  }

  /**
   *
   * @return
   */
  public String getTipo() {
    return tipo;
  }

  /**
   *
   * @param tipo
   */
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  /**
   *
   * @return
   */
  public Set<Platos> getPlato() {
    return plato;
  }

  /**
   *
   * @param plato
   */
  public void setPlato(Set<Platos> plato) {
    this.plato = plato;
  }

}
