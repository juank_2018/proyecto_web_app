/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Carta;

/**
 *
 * @author jcpm0
 */
public interface CartaService {

  /**
   *
   * @param c
   */
  public void addCarta(Carta c);

  /**
   *
   * @param c
   */
  public void updateCarta(Carta c);

  /**
   *
   * @return
   */
  public List<Carta> listCartas();

  /**
   *
   * @param offset
   * @param maxResults
   * @return
   */
  public List<Carta> listCartas(Integer offset, Integer maxResults);

  /**
   *
   * @param id
   * @return
   */
  public Carta getCartaById(int id);

  /**
   *
   * @param id
   */
  public void removeCarta(int id);
}
