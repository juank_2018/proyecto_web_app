/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Clientes;

/**
 *
 * @author jcpm0
 */
public interface ClientesService {

  /**
   *
   * @param c
   */
  public void addClientes(Clientes c);

  /**
   *
   * @param c
   */
  public void updateClientes(Clientes c);

  /**
   *
   * @return
   */
  public List<Clientes> listClientes();

  /**
   *
   * @param id
   * @return
   */
  public Clientes getClientesById(int id);

  /**
   *
   * @param id
   */
  public void removeClientes(int id);

  /**
   *
   * @param email
   * @return
   */
  public Clientes findClienteByEmail(String email);
}
