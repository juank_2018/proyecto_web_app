/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Clientes;
import spring.model.Dispositivos;

/**
 *
 * @author jcpm0
 */
public interface DispositivosService {

  /**
   *
   * @param c
   */
  public void addDispositivos(Dispositivos c);

  /**
   *
   * @param c
   */
  public void updateDispositivos(Dispositivos c);

  /**
   *
   * @return
   */
  public List<Dispositivos> listDispositivos();

  /**
   *
   * @param id
   * @return
   */
  public Dispositivos getDispositivosById(int id);

  /**
   *
   * @param id
   */
  public void removeDispositivos(int id);

  /**
   *
   * @param idCliente
   * @return
   */
  public List<Dispositivos> listaDispositivosCliente(int idCliente);

}
