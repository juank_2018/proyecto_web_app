/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Empleados;

/**
 *
 * @author jcpm0
 */
public interface EmpleadosService {

  /**
   *
   * @param c
   */
  public void addEmpleados(Empleados c);

  /**
   *
   * @param c
   */
  public void updateEmpleados(Empleados c);

  /**
   *
   * @return
   */
  public List<Empleados> listEmpleados();

  /**
   *
   * @param id
   * @return
   */
  public Empleados getEmpleadosById(int id);

  /**
   *
   * @param id
   */
  public void removeEmpleados(int id);

  /**
   *
   * @param login
   * @param paswd
   * @return
   */
  public Boolean LoginEmpleado(String login, String paswd);
  
  public List<Empleados> listarEmpleados(Integer offset, Integer maxResults);
}
