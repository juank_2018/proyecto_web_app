/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

/*
Servidor de correo saliente (SMTP) 	

smtp.gmail.com

Requiere SSL: sí

Requiere TLS: sí (si está disponible)

Requiere autenticación: sí

Puerto para SSL: 465

Puerto para TLS/STARTTLS: 587
 */
/**
 *
 * @author jcpm0
 */
public interface EnvioMailService {

  /**
   *
   * @param to
   * @param from
   * @param subject
   * @param text
   */
  public void send(String to, String from, String subject, String text);

}
