/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 *
 * @author jcpm0
 */
@Service
public class EnvioMailServiceImpl implements EnvioMailService {

  @Autowired
  private MailSender mailSender;

  /**
   *
   * @param to
   * @param from
   * @param subject
   * @param text
   */
  @Override
  public void send(String to, String from, String subject, String text) {
    SimpleMailMessage msg = new SimpleMailMessage();
    msg.setFrom(from);
    msg.setTo(to);
    msg.setSubject(subject);
    msg.setText(text);
    mailSender.send(msg);
  }

}
