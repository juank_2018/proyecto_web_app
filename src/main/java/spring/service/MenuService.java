/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.Date;
import java.util.List;
import spring.model.Menu;

/**
 *
 * @author jcpm0
 */
public interface MenuService {

  /**
   *
   * @param c
   */
  public void addMenu(Menu c);

  /**
   *
   * @param c
   */
  public void updateMenu(Menu c);

  /**
   *
   * @return
   */
  public List<Menu> listMenus();

  /**
   *
   * @param id
   * @return
   */
  public Menu getMenuById(int id);

  /**
   *
   * @param id
   */
  public void removeMenu(int id);

  /**
   *
   * @param offset
   * @param maxResults
   * @return
   */
  public List<Menu> listMenus(Integer offset, Integer maxResults);

  /**
   *
   * @param date
   * @return
   */
  public Menu getMenuByDate(String date);
}
