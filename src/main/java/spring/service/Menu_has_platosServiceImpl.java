/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.ArrayList;
import spring.DAO.Menu_has_platosDao;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.model.Menu_has_platos;
import org.springframework.transaction.annotation.Transactional;
import spring.DAO.Menu_has_platosDAOImpl;
import spring.model.Menu_has_platosId;

/**
 *
 * @author jcpm0
 */
@Service
public class Menu_has_platosServiceImpl implements Menu_has_platosSerice {

  private static final Logger LOG
          = Logger.getLogger(Menu_has_platosServiceImpl.class.getName());
  @Autowired
  private Menu_has_platosDao menu_has_plato;

  private void setMenu_has_platosDao(Menu_has_platosDao menu_has_plato) {
    this.menu_has_plato = menu_has_plato;
  }

  /**
   *
   * @param c
   */
  @Override
  @Transactional
  public void addMenu_has_platos(Menu_has_platos c) {
    LOG.info("entra en add service");
    this.menu_has_plato.addMenu_has_platos(c);
  }

  /**
   *
   * @param c
   */
  @Override
  @Transactional
  public void updateMenu_has_platos(Menu_has_platos c) {
    this.menu_has_plato.updateMenu_has_platos(c);
  }

  /**
   *
   * @return
   */
  @Override
  @Transactional
  public List<Menu_has_platos> listMenu_has_platos() {
    LOG.info("entra en list service");
    return this.menu_has_plato.listMenu_has_platos();
  }

  /**
   *
   * @param id
   * @return
   */
  @Override
  @Transactional
  public Menu_has_platos getMenu_has_platosById(Menu_has_platosId id) {
    return this.menu_has_plato.getMenu_has_platosById(id);
  }

  /**
   *
   * @param id
   */
  @Override
  @Transactional
  public void removeMenu_has_platos(Menu_has_platosId id) {
    this.menu_has_plato.removeMenu_has_platos(id);
  }

  /**
   *
   * @param id
   * @return
   */
  @Override
  @Transactional
  public List<Menu_has_platos> listMenus_has_platos(int id) {

    List<Menu_has_platos> lst = this.menu_has_plato.listMenu_has_platos();
    List<Menu_has_platos> result = new ArrayList<>();
    for (Menu_has_platos mhp : lst) {
      if (mhp.getMenu().getIdmenu() == id) {
        result.add(mhp);
      }
    }

    return result;
  }

  /**
   *
   * @param date
   * @return
   */
  @Override
  @Transactional
  public List<Menu_has_platos> getMenu_has_platosByDate(String date) {

    return menu_has_plato.getMenu_has_platosByDate(date);
  }
}
