/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Notificaciones;

/**
 *
 * @author jcpm0
 */
public interface NotificacionesService {

  /**
   *
   * @param c
   */
  public void addNotificaciones(Notificaciones c);

  /**
   *
   * @param c
   */
  public void updateNotificaciones(Notificaciones c);

  /**
   *
   * @return
   */
  public List<Notificaciones> listNotificaciones();

  /**
   *
   * @param id
   * @return
   */
  public Notificaciones getNotificacionesById(int id);

  /**
   *
   * @param id
   */
  public void removeNotificaciones(int id);

  /**
   *
   * @param tipo
   * @return
   */
  public List<Notificaciones> listNotificacionesPorTipo(boolean tipo);

  /**
   *
   * @param cliente
   * @param offset
   * @param maxResults
   * @return
   */
  public List<Notificaciones> listNotificaciones(boolean cliente, Integer offset, Integer maxResults);
}
