/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import spring.DAO.NotificacionesDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.model.Notificaciones;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jcpm0
 */
@Service
public class NotificacionesServiceImpl implements NotificacionesService {

  @Autowired
  private NotificacionesDao notificacion;

  private void setNotificacionesDao(NotificacionesDao notificacion) {
    this.notificacion = notificacion;
  }

  /**
   *
   * @param c
   */
  @Override
  @Transactional
  public void addNotificaciones(Notificaciones c) {
    this.notificacion.addNotificaciones(c);
  }

  /**
   *
   * @param c
   */
  @Override
  @Transactional
  public void updateNotificaciones(Notificaciones c) {
    this.notificacion.updateNotificaciones(c);
  }

  /**
   *
   * @return
   */
  @Override
  @Transactional
  public List<Notificaciones> listNotificaciones() {
    return this.notificacion.listNotificaciones();
  }

  /**
   *
   * @param id
   * @return
   */
  @Override
  @Transactional
  public Notificaciones getNotificacionesById(int id) {
    return this.notificacion.getNotificacionesById(id);
  }

  /**
   *
   * @param id
   */
  @Override
  @Transactional
  public void removeNotificaciones(int id) {
    this.notificacion.removeNotificaciones(id);
  }

  /**
   *
   * @param cliente
   * @param offset
   * @param maxResults
   * @return
   */
  @Override
  @Transactional
  public List<Notificaciones> listNotificaciones(boolean cliente, Integer offset, Integer maxResults) {
    return this.notificacion.listNotificaciones(cliente, offset, maxResults);
  }

  /**
   *
   * @param tipo
   * @return
   */
  @Override
  @Transactional
  public List<Notificaciones> listNotificacionesPorTipo(boolean tipo) {
    return this.notificacion.listNotificacionesPorTipo(tipo);
  }
}
