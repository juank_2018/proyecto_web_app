/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Platos;

/**
 *
 * @author jcpm0
 */
public interface PlatosService {

  /**
   *
   * @param c
   */
  public void addPlatos(Platos c);

  /**
   *
   * @param c
   */
  public void updatePlatos(Platos c);

  /**
   *
   * @return
   */
  public List<Platos> listPlatos();

  /**
   *
   * @param id
   * @return
   */
  public Platos getPlatosById(int id);

  /**
   *
   * @param id
   */
  public void removePlatos(int id);

  /**
   *
   * @return
   */
  public List<Platos> listaPlatosCarta();

  /**
   *
   * @param offset
   * @param maxResults
   * @return
   */
  public List<Platos> listaPlatosCarta(Integer offset, Integer maxResults);

  /**
   *
   * @return
   */
  public List<Platos> listarPlatosMenu();

  /**
   *
   * @param idCarta
   * @return
   */
  public List<Platos> platosQueEstanEnCarta(int idCarta);
}
