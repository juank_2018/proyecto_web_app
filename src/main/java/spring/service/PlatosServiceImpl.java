/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.ArrayList;
import java.util.HashSet;
import spring.DAO.PlatosDao;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.model.Platos;
import org.springframework.transaction.annotation.Transactional;
import spring.model.Platos_has_carta;
import spring.model.Tipo_plato;

/**
 *
 * @author jcpm0
 */
@Service
public class PlatosServiceImpl implements PlatosService {

  @Autowired
  private PlatosDao plato;
  @Autowired
  private Platos_has_cartaService phcService;

  private void setPlatosDao(PlatosDao plato) {
    this.plato = plato;
  }

  /**
   *
   * @param c
   */
  @Override
  @Transactional
  public void addPlatos(Platos c) {
    this.plato.addPlatos(c);
  }

  /**
   *
   * @param c
   */
  @Override
  @Transactional
  public void updatePlatos(Platos c) {
    this.plato.updatePlatos(c);
  }

  /**
   *
   * @return
   */
  @Override
  @Transactional
  public List<Platos> listPlatos() {
    return this.plato.listPlatos();
  }

  /**
   *
   * @param id
   * @return
   */
  @Override
  @Transactional
  public Platos getPlatosById(int id) {
    return this.plato.getPlatosById(id);
  }

  /**
   *
   * @param id
   */
  @Override
  @Transactional
  public void removePlatos(int id) {
    this.plato.removePlatos(id);
  }

  /**
   *
   * @param offset
   * @param maxResults
   * @return
   */
  @Transactional
  @Override
  public List<Platos> listaPlatosCarta(Integer offset, Integer maxResults) {
    return this.plato.listarPlatosCarta(offset, maxResults);
  }

  /**
   *
   * @return
   */
  @Transactional
  @Override
  public List<Platos> listaPlatosCarta() {
    return this.plato.listarPlatosCarta();
  }

  /**
   *
   * @return
   */
  @Transactional
  @Override
  public List<Platos> listarPlatosMenu() {
    return this.plato.listarPlatosMenu();
  }

  /**
   *
   * @param idCarta
   * @return
   */
  @Transactional
  @Override
  public List<Platos> platosQueEstanEnCarta(int idCarta) {
    List<Platos_has_carta> platosEnCarta = phcService.listPlatos_has_cartas(idCarta);
    List<Platos> result = new ArrayList<>();
    if (!platosEnCarta.isEmpty()) {
      for (Platos_has_carta p : platosEnCarta) {
        if (p.getAparece() == 1) {
          result.add(p.getPlato());
        }
      }
    } else {
      return null;
    }
    return result;
  }
}
