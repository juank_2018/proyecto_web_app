/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Reservas;

/**
 *
 * @author jcpm0
 */
public interface ReservasService {

  /**
   *
   * @param c
   */
  public void addReservas(Reservas c);

  /**
   *
   * @param c
   */
  public void updateReservas(Reservas c);

  /**
   *
   * @return
   */
  public List<Reservas> listReservas();

  /**
   *
   * @param id
   * @return
   */
  public List<Reservas> listReservas(int id);

  /**
   *
   * @param id
   * @return
   */
  public Reservas getReservasById(int id);

  /**
   *
   * @param id
   */
  public void removeReservas(int id);

  /**
   *
   * @param email
   * @return
   */
  public List<Reservas> listReservasByMail(String email);

  /**
   *
   * @param offset
   * @param maxResults
   * @return
   */
  public List<Reservas> listarReservas(Integer offset, Integer maxResults);
}
