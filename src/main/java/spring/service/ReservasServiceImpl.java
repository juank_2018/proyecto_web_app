/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.ArrayList;
import spring.DAO.ReservasDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import spring.model.Reservas;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jcpm0
 */
@Service
public class ReservasServiceImpl implements ReservasService {

  @Autowired
  private ReservasDao reserva;

  /**
   *
   * @param c
   */
  @Override
  @Transactional
  public void addReservas(Reservas c) {
    this.reserva.addReservas(c);
  }

  /**
   *
   * @param c
   */
  @Override
  @Transactional
  public void updateReservas(Reservas c) {
    this.reserva.updateReservas(c);
  }

  /**
   *
   * @return
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public List<Reservas> listReservas() {
    return this.reserva.listReservas();
  }

  /**
   *
   * @param id
   * @return
   */
  @Override
  @Transactional
  public Reservas getReservasById(int id) {
    return this.reserva.getReservasById(id);
  }

  /**
   *
   * @param id
   */
  @Override
  @Transactional
  public void removeReservas(int id) {
    this.reserva.removeReservas(id);
  }

  /**
   *
   * @param id
   * @return
   */
  @Override
  @Transactional
  public List<Reservas> listReservas(int id) {
    List<Reservas> lst = this.reserva.listReservas();
    List<Reservas> result = new ArrayList<>();

    lst.stream().filter((r) -> (r.getCliente().getIdclientes() == (long) id)).forEachOrdered((r) -> {
      result.add(r);
    });
    return result;
  }

  /**
   *
   * @param email
   * @return
   */
  @Override
  @Transactional
  public List<Reservas> listReservasByMail(String email) {
    return this.reserva.listReservasByMail(email);
  }

  /**
   *
   * @param offset
   * @param maxResults
   * @return
   */
  @Override
  @Transactional
  public List<Reservas> listarReservas(Integer offset, Integer maxResults) {
    return this.reserva.listarReservas(offset, maxResults);
  }
}
