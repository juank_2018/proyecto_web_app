/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Tipo_notif;

/**
 *
 * @author jcpm0
 */
public interface Tipo_notifService {

  /**
   *
   * @param c
   */
  public void addTipo_notif(Tipo_notif c);

  /**
   *
   * @param c
   */
  public void updateTipo_notif(Tipo_notif c);

  /**
   *
   * @return
   */
  public List<Tipo_notif> listTipo_notifs();

  /**
   *
   * @param id
   * @return
   */
  public Tipo_notif getTipo_notifById(int id);

  /**
   *
   * @param id
   */
  public void removeTipo_notif(int id);
}
