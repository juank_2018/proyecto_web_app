/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.service;

import java.util.List;
import spring.model.Usuarios;

/**
 *
 * @author jcpm0
 */
public interface UsuariosService {

  /**
   *
   * @param c
   */
  public void addUsuarios(Usuarios c);

  /**
   *
   * @param c
   */
  public void updateUsuarios(Usuarios c);

  /**
   *
   * @return
   */
  public List<Usuarios> listUsuarios();

  /**
   *
   * @param id
   * @return
   */
  public Usuarios getUsuariosById(int id);

  /**
   *
   * @param id
   */
  public void removeUsuarios(int id);
}
