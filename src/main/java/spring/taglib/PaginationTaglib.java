/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.taglib;

/**
 *
 * @author jcpm0
 */
import java.io.Writer;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author jcpm0
 */
public class PaginationTaglib extends SimpleTagSupport {

  private String uri;
  private int offset;
  private int count;
  private int max = 10;
  private int steps = 10;
  private String previous = "Previous";
  private String next = "Next";

  private Writer getWriter() {
    JspWriter out = getJspContext().getOut();
    return out;
  }

  /**
   *
   * @throws JspException
   */
  @Override
  public void doTag() throws JspException {
    Writer out = getWriter();

    try {
      out.write("<nav>");
      out.write("<ul class=\"pagination\">");

      if (offset < steps) {
        out.write(constructLink(1, previous, "disabled", true));
      } else {
        out.write(constructLink(offset - steps, previous, null, false));
      }

      for (int itr = 0; itr < count; itr += steps) {
        if (offset == itr) {
          out.write(constructLink((itr / 10 + 1) - 1 * steps, String.valueOf(itr / 10 + 1), "active", true));
        } else {
          out.write(constructLink(itr / 10 * steps, String.valueOf(itr / 10 + 1), null, false));
        }
      }

      if (offset + steps >= count) {
        out.write(constructLink(offset + steps, next, "disabled", true));
      } else {
        out.write(constructLink(offset + steps, next, null, false));
      }

      out.write("</ul>");
      out.write("</nav>");
    } catch (java.io.IOException ex) {
      throw new JspException("Error in Paginator tag", ex);
    }
  }

  private String constructLink(int page, String text, String className, boolean disabled) {
    StringBuilder link = new StringBuilder("<li");
    if (className != null) {
      link.append(" class=\"page-item\" \"");
      link.append(className);
      link.append("\"");
    }
    if (disabled) {
      link.append(">").append("<a class=\"page-link\"  href=\"#\">" + text + "</a></li>");
    } else {
      link.append(">").append("<a class=\"page-link\" href=\"" + uri + "?offset=" + page + "\">" + text + "</a></li>");
    }
    return link.toString();
  }

  /**
   *
   * @return
   */
  public String getUri() {
    return uri;
  }

  /**
   *
   * @param uri
   */
  public void setUri(String uri) {
    this.uri = uri;
  }

  /**
   *
   * @return
   */
  public int getOffset() {
    return offset;
  }

  /**
   *
   * @param offset
   */
  public void setOffset(int offset) {
    this.offset = offset;
  }

  /**
   *
   * @return
   */
  public int getCount() {
    return count;
  }

  /**
   *
   * @param count
   */
  public void setCount(int count) {
    this.count = count;
  }

  /**
   *
   * @return
   */
  public int getMax() {
    return max;
  }

  /**
   *
   * @param max
   */
  public void setMax(int max) {
    this.max = max;
  }

  /**
   *
   * @return
   */
  public String getPrevious() {
    return previous;
  }

  /**
   *
   * @param previous
   */
  public void setPrevious(String previous) {
    this.previous = previous;
  }

  /**
   *
   * @return
   */
  public String getNext() {
    return next;
  }

  /**
   *
   * @param next
   */
  public void setNext(String next) {
    this.next = next;
  }

  /**
   *
   * @return
   */
  public int getSteps() {
    return steps;
  }

  /**
   *
   * @param steps
   */
  public void setSteps(int steps) {
    this.steps = steps;
  }

}
