/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.utils;

/**
 *
 * @author jcpm0
 */
public class CartaPlato {

  String idPlato;
  String idCarta;
  String aparece;

  /**
   *
   */
  public CartaPlato() {
  }

  /**
   *
   * @return
   */
  public String getIdPlato() {
    return idPlato;
  }

  /**
   *
   * @param idPlato
   */
  public void setIdPlato(String idPlato) {
    this.idPlato = idPlato;
  }

  /**
   *
   * @return
   */
  public String getIdCarta() {
    return idCarta;
  }

  /**
   *
   * @param idCarta
   */
  public void setIdCarta(String idCarta) {
    this.idCarta = idCarta;
  }

  /**
   *
   * @return
   */
  public String getAparece() {
    return aparece;
  }

  /**
   *
   * @param aparece
   */
  public void setAparece(String aparece) {
    this.aparece = aparece;
  }

}
