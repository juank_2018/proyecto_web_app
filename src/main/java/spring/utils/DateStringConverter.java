/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.core.convert.converter.Converter;

/**
 * Clase para convertir Objetos Date a String
 * @author jcpm0
 */
public class DateStringConverter implements Converter<Date, String> {

  /**
   *
   * @param s
   * @return
   */
  @Override
  public String convert(Date s) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    return sdf.format(s);
  }

}
