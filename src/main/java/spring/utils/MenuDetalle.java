/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import spring.model.Menu;

/**
 * Clase auxiliar usada en MenuController, va a permitir tener en un objeto
 * el menu, y 3 listas con los platos de cada sección del menu.
 * Se usa para pasar los datos a la vista y que sea más fácil poblar los select.
 * @author jcpm0
 */
public class MenuDetalle {

  private List<Plato> primeros = new ArrayList<>();
  private List<Plato> segundos = new ArrayList<>();
  private List<Plato> postres = new ArrayList<>();
  private Menu menu;

  /**
   *
   * @return
   */
  public Menu getMenu() {
    return menu;
  }

  /**
   *
   * @param menu
   */
  public void setMenu(Menu menu) {
    this.menu = menu;
  }

  @Override
  public String toString() {
    return "MenuDetalle{" + "primeros=" + primeros + ", segundos=" + segundos + ", postres=" + postres + ", menu=" + menu + '}';
  }

  /**
   *
   * @return
   */
  public List<Plato> getPrimeros() {
    return primeros;
  }

  /**
   *
   * @param primeros
   */
  public void setPrimeros(List<Plato> primeros) {
    this.primeros = primeros;
  }

  /**
   *
   * @return
   */
  public List<Plato> getSegundos() {
    return segundos;
  }

  /**
   *
   * @param segundos
   */
  public void setSegundos(List<Plato> segundos) {
    this.segundos = segundos;
  }

  /**
   *
   * @return
   */
  public List<Plato> getPostres() {
    return postres;
  }

  /**
   *
   * @param postres
   */
  public void setPostres(List<Plato> postres) {
    this.postres = postres;
  }

  /**
   *
   */
  public static class Plato {

    private String plato;
    private int idplato;

    /**
     *
     * @return
     */
    public int getIdplato() {
      return idplato;
    }

    /**
     *
     * @param idplato
     */
    public void setIdplato(int idplato) {
      this.idplato = idplato;
    }

    @Override
    public String toString() {
      return "Plato{" + "plato=" + plato + ", idplato=" + idplato + '}';
    }

    /**
     *
     */
    public Plato() {
    }

    /**
     *
     * @return
     */
    public String getPlato() {
      return plato;
    }

    /**
     *
     * @param plato
     */
    public void setPlato(String plato) {
      this.plato = plato;
    }

  }

}
