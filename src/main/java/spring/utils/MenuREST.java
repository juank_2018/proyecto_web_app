/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jcpm0
 */
public class MenuREST {

  private String tipo;

  private List<String> platos = new ArrayList<>();

  /**
   *
   * @return
   */
  public List<String> getPlatos() {
    return platos;
  }

  /**
   *
   * @param platos
   */
  public void setPlatos(List<String> platos) {
    this.platos = platos;
  }

  /**
   *
   * @param tipo
   * @param platos
   */
  public MenuREST(String tipo, List<String> platos) {
    this.tipo = tipo;
    this.platos = platos;
  }

  /**
   *
   */
  public MenuREST() {
  }

  /**
   *
   * @return
   */
  public String getTipo() {
    return tipo;
  }

  /**
   *
   * @param tipo
   */
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

}
