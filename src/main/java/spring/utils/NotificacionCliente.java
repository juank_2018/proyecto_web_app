/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.utils;

import spring.model.Clientes;
import spring.model.Notificaciones;

/**
 * Clase auxiliar que permite tener en un objeto al cliente y su notificacion
 * Será util para pasar datos a la vista
 * @author jcpm0
 */
public class NotificacionCliente {

  Clientes cliente;
  Notificaciones notificacion;

  /**
   *
   */
  public NotificacionCliente() {
  }

  /**
   *
   * @return
   */
  public Clientes getCliente() {
    return cliente;
  }

  /**
   *
   * @param cliente
   */
  public void setCliente(Clientes cliente) {
    this.cliente = cliente;
  }

  /**
   *
   * @return
   */
  public Notificaciones getNotificacion() {
    return notificacion;
  }

  /**
   *
   * @param notificacion
   */
  public void setNotificacion(Notificaciones notificacion) {
    this.notificacion = notificacion;
  }

}
