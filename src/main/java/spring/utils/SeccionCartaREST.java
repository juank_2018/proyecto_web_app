/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.utils;

import java.util.List;
import spring.model.Platos;

/**
 *
 * @author jcpm0
 */
public class SeccionCartaREST {

  private String tipoPlato;
  private int idTipoPlato;
  private List<Platos> platosSeccion;

  /**
   *
   */
  public SeccionCartaREST() {
  }

  /**
   *
   * @param tipoPlato
   * @param idTipoPlato
   * @param platosSeccion
   */
  public SeccionCartaREST(String tipoPlato, int idTipoPlato, List<Platos> platosSeccion) {
    this.tipoPlato = tipoPlato;
    this.idTipoPlato = idTipoPlato;
    this.platosSeccion = platosSeccion;
  }

  @Override
  public String toString() {
    return "SeccionCartaREST{" + "tipoPlato=" + tipoPlato + ", idTipoPlato=" + idTipoPlato + ", platosSeccion=" + platosSeccion + '}';
  }

  /**
   *
   * @return
   */
  public String getTipoPlato() {
    return tipoPlato;
  }

  /**
   *
   * @param tipoPlato
   */
  public void setTipoPlato(String tipoPlato) {
    this.tipoPlato = tipoPlato;
  }

  /**
   *
   * @return
   */
  public int getIdTipoPlato() {
    return idTipoPlato;
  }

  /**
   *
   * @param idTipoPlato
   */
  public void setIdTipoPlato(int idTipoPlato) {
    this.idTipoPlato = idTipoPlato;
  }

  /**
   *
   * @return
   */
  public List<Platos> getPlatosSeccion() {
    return platosSeccion;
  }

  /**
   *
   * @param platosSeccion
   */
  public void setPlatosSeccion(List<Platos> platosSeccion) {
    this.platosSeccion = platosSeccion;
  }

}
