/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.utils;

/**
 *
 * @author jcpm0
 */
public class SimpleResponse {

  private String message;

  private boolean error;

  /**
   *
   */
  public SimpleResponse() {
  }

  /**
   *
   * @return
   */
  public String getMessage() {
    return message;
  }

  /**
   *
   * @param message
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   *
   * @return
   */
  public boolean isError() {
    return error;
  }

  /**
   *
   * @param error
   */
  public void setError(boolean error) {
    this.error = error;
  }
}
