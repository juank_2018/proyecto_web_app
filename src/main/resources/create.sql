-- MySQL Script generated by MySQL Workbench
-- Sat Feb 17 13:40:57 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema proyecto_final
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema proyecto_final
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `proyecto_final` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema test
-- -----------------------------------------------------
USE `proyecto_final` ;

-- -----------------------------------------------------
-- Table `proyecto_final`.`carta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`carta` (
  `idcarta` INT(11) NOT NULL AUTO_INCREMENT,
  `noombre` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`idcarta`))
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`usuarios` (
  `idusuarios` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NOT NULL,
  `apellido1` VARCHAR(20) NOT NULL,
  `apellido2` VARCHAR(20) NULL DEFAULT NULL,
  `fechaNacimiento` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`idusuarios`))
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`clientes` (
  `idclientes` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `paswd` VARCHAR(8) NOT NULL,
  `usuarios_idusuarios` INT(11) NOT NULL,
  PRIMARY KEY (`idclientes`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_clientes_usuarios1_idx` (`usuarios_idusuarios` ASC),
  CONSTRAINT `fk_clientes_usuarios1`
    FOREIGN KEY (`usuarios_idusuarios`)
    REFERENCES `proyecto_final`.`usuarios` (`idusuarios`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`dispositivos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`dispositivos` (
  `iddispositivos` INT(11) NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(255) NOT NULL,
  `clientes_idclientes` INT(11) NOT NULL,
  PRIMARY KEY (`iddispositivos`),
  INDEX `fk_dispositivos_clientes1_idx` (`clientes_idclientes` ASC),
  CONSTRAINT `fk_dispositivos_clientes1`
    FOREIGN KEY (`clientes_idclientes`)
    REFERENCES `proyecto_final`.`clientes` (`idclientes`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`notificaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`notificaciones` (
  `idnotificaciones` INT(11) NOT NULL AUTO_INCREMENT,
  `mensaje` VARCHAR(255) NOT NULL,
  `fecha` DATE NOT NULL,
  `entregada` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`idnotificaciones`))
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`dispositivos_has_notificaciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`dispositivos_has_notificaciones` (
  `dispositivos_iddispositivos` INT(11) NOT NULL,
  `notificaciones_idnotificaciones` INT(11) NOT NULL,
  PRIMARY KEY (`dispositivos_iddispositivos`, `notificaciones_idnotificaciones`),
  INDEX `fk_dispositivos_has_notificaciones_notificaciones1_idx` (`notificaciones_idnotificaciones` ASC),
  INDEX `fk_dispositivos_has_notificaciones_dispositivos1_idx` (`dispositivos_iddispositivos` ASC),
  CONSTRAINT `fk_dispositivos_has_notificaciones_dispositivos1`
    FOREIGN KEY (`dispositivos_iddispositivos`)
    REFERENCES `proyecto_final`.`dispositivos` (`iddispositivos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_dispositivos_has_notificaciones_notificaciones1`
    FOREIGN KEY (`notificaciones_idnotificaciones`)
    REFERENCES `proyecto_final`.`notificaciones` (`idnotificaciones`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`empleados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`empleados` (
  `idempleados` INT(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(10) NOT NULL,
  `paswd` VARCHAR(8) NOT NULL,
  `rol` VARCHAR(20) NULL DEFAULT NULL,
  `usuarios_idusuarios` INT(11) NOT NULL,
  PRIMARY KEY (`idempleados`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  INDEX `fk_empleados_usuarios1_idx` (`usuarios_idusuarios` ASC),
  CONSTRAINT `fk_empleados_usuarios1`
    FOREIGN KEY (`usuarios_idusuarios`)
    REFERENCES `proyecto_final`.`usuarios` (`idusuarios`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`menu` (
  `idmenu` INT(11) NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  PRIMARY KEY (`idmenu`))
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`platos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`platos` (
  `idPlato` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  `precioTapa` DECIMAL(3,2) NOT NULL,
  `precioMedia` DECIMAL(3,2) NOT NULL,
  `precioRacion` DECIMAL(3,2) NOT NULL,
  `empleados_idempleados` INT(11) NULL,
  PRIMARY KEY (`idPlato`),
  INDEX `fk_platos_empleados1_idx` (`empleados_idempleados` ASC),
  CONSTRAINT `fk_platos_empleados1`
    FOREIGN KEY (`empleados_idempleados`)
    REFERENCES `proyecto_final`.`empleados` (`idempleados`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`menu_has_platos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`menu_has_platos` (
  `menu_idmenu` INT(11) NOT NULL,
  `platos_idPlato` INT(11) NOT NULL,
  `tipo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`menu_idmenu`, `platos_idPlato`),
  INDEX `fk_menu_has_platos_platos1_idx` (`platos_idPlato` ASC),
  INDEX `fk_menu_has_platos_menu1_idx` (`menu_idmenu` ASC),
  CONSTRAINT `fk_menu_has_platos_menu1`
    FOREIGN KEY (`menu_idmenu`)
    REFERENCES `proyecto_final`.`menu` (`idmenu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_menu_has_platos_platos1`
    FOREIGN KEY (`platos_idPlato`)
    REFERENCES `proyecto_final`.`platos` (`idPlato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`tipo_notif`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`tipo_notif` (
  `idtipo_notif` INT(11) NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idtipo_notif`))
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`notif_sistema`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`notif_sistema` (
  `idnotif_sistema` INT(11) NOT NULL AUTO_INCREMENT,
  `tipo_notif_idtipo_notif` INT(11) NOT NULL,
  `notificaciones_idnotificaciones` INT(11) NOT NULL,
  PRIMARY KEY (`idnotif_sistema`),
  INDEX `fk_notif_sistema_tipo_notif1_idx` (`tipo_notif_idtipo_notif` ASC),
  INDEX `fk_notif_sistema_notificaciones1_idx` (`notificaciones_idnotificaciones` ASC),
  CONSTRAINT `fk_notif_sistema_notificaciones1`
    FOREIGN KEY (`notificaciones_idnotificaciones`)
    REFERENCES `proyecto_final`.`notificaciones` (`idnotificaciones`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_notif_sistema_tipo_notif1`
    FOREIGN KEY (`tipo_notif_idtipo_notif`)
    REFERENCES `proyecto_final`.`tipo_notif` (`idtipo_notif`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`platos_has_carta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`platos_has_carta` (
  `platos_idPlato` INT(11) NOT NULL,
  `carta_idcarta` INT(11) NOT NULL,
  `aparece` TINYINT(1) NOT NULL,
  PRIMARY KEY (`platos_idPlato`, `carta_idcarta`),
  INDEX `fk_platos_has_carta_carta1_idx` (`carta_idcarta` ASC),
  INDEX `fk_platos_has_carta_platos_idx` (`platos_idPlato` ASC),
  CONSTRAINT `fk_platos_has_carta_carta1`
    FOREIGN KEY (`carta_idcarta`)
    REFERENCES `proyecto_final`.`carta` (`idcarta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_platos_has_carta_platos`
    FOREIGN KEY (`platos_idPlato`)
    REFERENCES `proyecto_final`.`platos` (`idPlato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`tipo_plato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`tipo_plato` (
  `idTipo` INT(11) NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(12) NULL DEFAULT NULL,
  PRIMARY KEY (`idTipo`))
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`platos_has_tipo_plato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`platos_has_tipo_plato` (
  `platos_idPlato` INT(11) NOT NULL,
  `tipo_plato_idTipo` INT(11) NOT NULL,
  PRIMARY KEY (`platos_idPlato`, `tipo_plato_idTipo`),
  INDEX `fk_platos_has_tipo_plato_tipo_plato1_idx` (`tipo_plato_idTipo` ASC),
  INDEX `fk_platos_has_tipo_plato_platos1_idx` (`platos_idPlato` ASC),
  CONSTRAINT `fk_platos_has_tipo_plato_platos1`
    FOREIGN KEY (`platos_idPlato`)
    REFERENCES `proyecto_final`.`platos` (`idPlato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_platos_has_tipo_plato_tipo_plato1`
    FOREIGN KEY (`tipo_plato_idTipo`)
    REFERENCES `proyecto_final`.`tipo_plato` (`idTipo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `proyecto_final`.`reservas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto_final`.`reservas` (
  `idreservas` INT(11) NOT NULL AUTO_INCREMENT,
  `nComensales` INT(11) NOT NULL,
  `turno` VARCHAR(10) NOT NULL,
  `fechaHora` DATETIME NOT NULL,
  `clientes_idclientes` INT(11) NOT NULL,
  PRIMARY KEY (`idreservas`),
  INDEX `fk_reservas_clientes1_idx` (`clientes_idclientes` ASC),
  CONSTRAINT `fk_reservas_clientes1`
    FOREIGN KEY (`clientes_idclientes`)
    REFERENCES `proyecto_final`.`clientes` (`idclientes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
