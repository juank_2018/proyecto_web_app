<%-- 
    Document   : carta
    Created on : 27-feb-2018, 20:20:38
    Author     : jcpm0
    Vista para mostrar las cartas que tenga el restaurante.
    Recibe la lista de cartas en la variable cartes desde el controlador.
    Incorpora un formulario para crear una nueva carta.
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tag" uri="/WEB-INF/jsp/taglib/customTaglib.tld"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cartas</title>
    <meta  charset="UTF-8">
    <meta name="viewport" content="width=device-width,
          initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-foundation.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/estilos.css" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.js" ></script>
    <script src="${pageContext.request.contextPath}/resources/js/popper.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.validate.js"></script>

    <!-- Validador JQuery se encarga de validar el campo del formulario -->
    <script type="text/javascript">
      $(document).ready(function () {
        $('#crearCarta').validate({
          rules: {
            noombre: {
              required: true
            }
          },
          messages: {
            noombre: {
              required: "El campo nombre es obligatorio"
            }
          },
          errorClass: "is-invalid"
        });
      });
    </script>
  </head>
  <body>
    <div class=" container"><!-- container -->
      <header class="row justify-content-md-endr ">
        <div class="col "><!-- columna única para la barra de navegación -->
          <nav class="navbar navbar-expand-sm navbar-dark bg-primary ">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/inicio">#BienMeSabe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
              <ul class="navbar-nav   ">
                <li class="nav-item">
                  <a class="nav-link  " href="${pageContext.request.contextPath}/empleados">Empleados</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/platos">Platos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active " href="${pageContext.request.contextPath}/carta">Carta</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/menu">Menú del día</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/inicio">Reservas</a>
                </li>
                <li class="nav-item dropdown ">
                  <a class="nav-link dropdown-toggle" href="#" 
                     id="navbarDropdownPlatos" role="button" 
                     data-toggle="dropdown" aria-haspopup="true" 
                     aria-expanded="false">Notificaciones</a>
                  <div class="dropdown-menu" 
                       aria-labelledby="navbarDropdownPlatos">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificaciones">Sistema</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificacionesClientes">Cliente</a>

                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/spring/logout"  >Salir</a>
                </li>

              </ul>
            </div>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  <sec:authorize access="isAuthenticated()"  >
                    usuario:  <sec:authentication property="principal.username" /> 
                  </sec:authorize>
                </a>
              </li>
            </ul> 
          </nav>
        </div><!-- columna única para la barra de navegación -->
      </header>
      <main class="row justify-content-md-center ">

        <div class="col-md-auto">
          <div class="card "><!-- card -->
            <div class="card-header"> <!-- card header -->
              <h5 >Cartas</h5>
            </div><!-- card header -->
            <div class="card-body"><!-- card boy -->
              <div class="row align-items-start">
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                  <!-- formulario para crear una carta -->
                  <form:form  method="POST" action="/spring/crearCarta" id="crearCarta" modelAttribute="carta"  >
                    <div class=" row justify-content-end ">
                      <div class="form-group col-md-9"> 
                        <label class="form-text" for="nombre">Nombre</label>
                        <form:input  class="form-control form-control-sm " id="nombre" path="noombre"   ></form:input>
                        </div>
                        <div class="form-group col-md-3 align-self-end ">
                          <button type="submit" id="save"    class="btn btn-info btn-sm ">Crear carta </button>
                        </div>
                      </div>
                  </form:form>

                </sec:authorize>

              </div>
              <div class="row table-responsive-md"> <!-- Tabla -->
                <table class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th scope="row">Id</th>
                      <th scope="row">Nonbre</th>
                       <th scope="row">Editar</th>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">

                       
                        <th scope="row">Eliminar</th>
                        </sec:authorize>

                    </tr>
                  </thead>
                  <tbody>
                    <!-- para rellenar la tabla uso el tag forEach que pertenece a la libreria JSLT
                    y que permite iterar sobre los valores que se pasan en una varizble de tipo lista-->
                    <c:forEach items="${cartas}" var="consulta" >
                      <tr>
                        <th scope="row"><c:out value="${consulta.idcarta}"></c:out> </th>
                        <td><c:out value="${consulta.noombre}"></c:out></td>
                           <td><a href="editaCarta/${consulta.idcarta}" class="btn btn-info btn-xs">
                              <i class="oi oi-pencil"></i> Editar </a>
                          </td>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">

                       
                          <td><a href="borraCarta/${consulta.idcarta}" class="btn btn-danger btn-xs"> <span class="oi oi-delete "></span> Eliminar</a> </td>
                        </sec:authorize>

                      </tr>
                    </c:forEach>
                  </tbody>
                </table>
              </div><!-- Tabla -->
              <tag:paginate max="10" offset="${offset}" count="${count}" uri="carta.htm" next="&raquo;" previous="&laquo;" />
            </div> <!-- card boy -->
          </div><!-- card -->
        </div><!-- col -->

      </main>



    </div><!-- Fin contenedor -->
  </body>
</html>
