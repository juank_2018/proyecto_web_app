<%-- 
    Document   : creaEmpleado
    Created on : 02-may-2018, 20:16:53
    Author     : jcpm0
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" uri="/WEB-INF/jsp/taglib/customTaglib.tld"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
  <head>
    <meta  charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-foundation.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/estilos.css" type="text/css">
   <script src="resources/js/jquery-3.3.1.js" ></script>

    <script src="${pageContext.request.contextPath}/resources/js/popper.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.validate.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>
    <title>Empleados</title>
    <script type="text/javascript">

      $(document).ready(function () {
        $('#creaEmpleado').validate({
          rules: {
            login: {
              required: true
            },
            paswd: {
              required: true
            },
            datepicker: {
              required: true
              
            },
            "usuario.nombre":{
              required:true
            },
            "usuario.apellido1":{
              required:true
            },
            "usuario.apellido2":{
              required:true
            },
            rol:{
              required:true
            }
          },
          messages: {
        login: {
              required: "campo requerido"
            },
            paswd: {
              required: "campo requerido"
            },
            datepicker: {
              required: "campo requerido"
              
            },
            "usuario.nombre":{
              required:"campo requerido"
            },
            "usuario.apellido1":{
              required:"campo requerido"
            },
            "usuario.apellido2":{
              required:"campo requerido"
            },
            rol:{
              required:"campo requerido"
            }
          },
          errorClass: "is-invalid"
        });
      });
    </script>
        <!-- Datepicker, script que permite usar un calendario para seleccionar las fechas -->
    <script type="text/javascript">
      jQuery(function ($) {
        $.datepicker.regional['es'] = {
          closeText: 'Cerrar',
          prevText: '&#x3c;Ant',
          nextText: 'Sig&#x3e;',
          currentText: 'Hoy',
          monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
          dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
          dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
          dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
          weekHeader: 'Sm',
          dateFormat: 'dd/mm/yy',
          firstDay: 1,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['es']);
      });
      
      <%-- altField permite que el resultado del datepicker se escriba en un 
campo alternativo y altFormat permite cambiar el formato, es útil porque para el 
usuario es correcto la forma en la que presenta la fecha datepicker  dd-mm-yyyy
pero para insertar en la base de datos necesito que sea yyy-mm-dd, con altRormat
lo pongo así, y con altField coloco la fecha en un campo oculto que es el 
que uso para pasr en el post.minDate permite restringir los dias que se pueden
seleccionar en el calendario, el valor 0 indica que no perimite  ningún día
anterior a la fecha actual --%>

      $(document).ready(function () {
        $("#datepicker").datepicker({
          altField: "#alternate",
          altFormat: "dd/mm/yy",
               changeMonth: true,
      changeYear: true,
      yearRange: "c-70:c+00"
        });
      });
     </script>
  </head>
  <body>
    <div class="container ">
      <header class="row justify-content-md-endr ">
        <div class="col "><!-- columna única para la barra de navegación -->
          <nav class="navbar navbar-expand-sm navbar-dark bg-primary ">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/inicio">#BienMeSabe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
              <ul class="navbar-nav   ">
                <li class="nav-item">
                  <a class="nav-link active " href="${pageContext.request.contextPath}/empleados">Empleados</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/platos">Platos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/carta">Carta</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/menu">Menú del día</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/inicio">Reservas</a>
                </li>
                <li class="nav-item dropdown ">
                  <a class="nav-link dropdown-toggle" href="#" 
                     id="navbarDropdownPlatos" role="button" 
                     data-toggle="dropdown" aria-haspopup="true" 
                     aria-expanded="false">Notificaciones</a>
                  <div class="dropdown-menu" 
                       aria-labelledby="navbarDropdownPlatos">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificaciones">Sistema</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificacionesClientes">Cliente</a>

                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/spring/logout"  >Salir</a>
                </li>

              </ul>
            </div>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  <sec:authorize access="isAuthenticated()" >
                    usuario:  <sec:authentication property="principal.username" /> 
                  </sec:authorize>
                </a>
              </li>
            </ul> 
          </nav>
        </div><!-- columna única para la barra de navegación -->
      </header>
      <main class="row justify-content-md-center ">
        <div class="col-md-8 ">
          <div class="card ">
            <h5 class="card-header">Crear Empleado</h5>

            <div class="card-body">

              <div class="row justify-content-center ">
                <div class="col-md-8 ">
                  <c:if test="${msg != null}" >
                    <p class="alert alert-danger"><c:out value="${msg}"></c:out></p>
                  </c:if>
                </div>


              </div>
              <form:form  method="post" 
                          action="creaEmpleado" id="creaEmpleado"  modelAttribute="empleado" >
                <div class="form-row">
                       
                  <div class=" form-group col-md-6">
                    <label for="login">Login</label>
                    <form:input class="form-control form-control-sm " id="login" path="login" />

                  </div>
                  <div class=" form-group col-md-6">
                    <label for="paswd">Pasword</label>
                    <form:input class="form-control form-control-sm" id="paswd" path="paswd" />
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-4 form-group">
                    <label for="usuario.nombre">Nombre</label>
                    <form:input class="form-control form-control-sm " path="usuario.nombre"/>
                  </div>
                  <div class="col-md-4 form-group">
                    <label for="usuario.apellido1">Primer apellido</label>
                    <form:input class="form-control form-control-sm " path="usuario.apellido1"/>
                  </div>
                  <div class="col-md-4 form-group">
                    <label for="usuario.apellido2">Segundo apellido</label>
                    <form:input class="form-control form-control-sm " path="usuario.apellido2"/>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-6 form-group">
                    <label for="usuario.fechanacimiento">Fecha de nacimiento</label>
                    
                       <form:hidden path="usuario.fechanacimiento"  id="alternate" ></form:hidden>
                      <input type="text" name="datepicker" class="form-control form-control-sm" id="datepicker" readonly="readonly" size="12" /> 
                  </div>
                  <div class="col-md-6 form-group">
                      <label for="rol ">Rol</label>
                    <form:select id="rol" path="rol" class="form-control form-control-sm"  >
                      <form:option value="">Selecciona rol...</form:option>
                      <form:option value="ROL_ADMIN">Admin</form:option>
                      <form:option value="ROL_USER">empleado</form:option>
                    </form:select>
                  </div>
                
                </div>
                <div class="form-row  "  >
                  <div class="form-group col-md-8"></div>
                  <div class="form-group col-md-auto  ">
                    <button type="reset" class=" btn btn-secondary">Cancelar</button>
                  </div>
                  <div class="form-group col-md-auto">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
                </div>
              </form:form>


            </div>
          </div>
        </div>
      </main>
    </div>
  </body>
</html>
