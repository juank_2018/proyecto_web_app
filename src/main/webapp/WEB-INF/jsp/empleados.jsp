<%-- 
    Document   : empleados
    Created on : 02-may-2018, 17:06:55
    Author     : jcpm0
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" uri="/WEB-INF/jsp/taglib/customTaglib.tld"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
  <head>
    <meta  charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-foundation.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/estilos.css" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.js" ></script>
    <script src="${pageContext.request.contextPath}/resources/js/popper.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.validate.js"></script>

    <title>Empleados</title>
  </head>
  <body>
    <div class="container ">
      <header class="row justify-content-md-endr ">
        <div class="col "><!-- columna única para la barra de navegación -->
          <nav class="navbar navbar-expand-sm navbar-dark bg-primary ">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/inicio">#BienMeSabe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
              <ul class="navbar-nav   ">
                <li class="nav-item">
                  <a class="nav-link active " href="${pageContext.request.contextPath}/empleados">Empleados</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/platos">Platos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/carta">Carta</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/menu">Menú del día</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/inicio">Reservas</a>
                </li>
                <li class="nav-item dropdown ">
                  <a class="nav-link dropdown-toggle" href="#" 
                     id="navbarDropdownPlatos" role="button" 
                     data-toggle="dropdown" aria-haspopup="true" 
                     aria-expanded="false">Notificaciones</a>
                  <div class="dropdown-menu" 
                       aria-labelledby="navbarDropdownPlatos">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificaciones">Sistema</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificacionesClientes">Cliente</a>

                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/spring/logout"  >Salir</a>
                </li>

              </ul>
            </div>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  <sec:authorize access="isAuthenticated()" >
                    usuario:  <sec:authentication property="principal.username" /> 
                  </sec:authorize>
                </a>
              </li>
            </ul> 
          </nav>
        </div><!-- columna única para la barra de navegación -->
      </header>
      <main class="row justify-content-md-center ">
        <div class="col-md-8 ">
          <div class="card ">
            <h5 class="card-header">Empleados</h5>

            <div class="card-body">
              <div class="row align-items-start">
                <div class="col-8"></div>
                <div class="col-md-2">
                  <sec:authorize access="hasRole('ROLE_ADMIN')">
                     <a href="crearEmpleado" class="mb-3 btn btn-info btn-xs"> 
                    <i class="oi oi-pencil"></i> Crear Empleado
                  </a>
                  </sec:authorize>
                            
                </div>
              </div>
              <div class="row justify-content-center ">
                <div class="col-md-8 ">
                  <c:if test="${msg != null}" >
                    <p class="alert alert-danger"><c:out value="${msg}"></c:out></p>
                  </c:if>
                </div>


              </div>

              <table class="table-responsive-md table-sm table-hover table-striped">
                <thead>
                  <tr>
                    <th class="text-center" scope="row">Id</th>
                    <th class="text-center" scope="row">Nombre</th>
                    <th class="text-center" scope="row">Apellidos</th>
                    <th class="text-center" scope="row">Fecha Nacimiento</th>
                    <th class="text-center" scope="row">Login</th>
                    <th class="text-center" scope="row">Rol</th>
                    <th class="text-center" scope="row">Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                  <!-- Recprro el item reservas para popular la tabla -->
                  <c:forEach items="${empleados}" var="consulta">
                    <tr>
                      <th scope="row"><c:out value="${consulta.idempleados}"></c:out></th>
                      <td class="text-center"><c:out value="${consulta.usuario.nombre}"></c:out></td>
                      <td class="text-center"><c:out value="${consulta.usuario.apellido1} ${consulta.usuario.apellido2} "></c:out></td>
                        <td class="text-center">

                        <c:out value="${consulta.usuario.fechanacimiento}"></c:out>
                        </td>
                        <td>

                        <c:out value="${consulta.login}"></c:out>
                        </td>
                        <td class="text-center"><c:out value="${consulta.rol}"></c:out></td>
                        <td class="text-center">
                          <a href="borraEmpleado/${consulta.idempleados}" class="btn btn-danger btn-sm"> <span class="oi oi-trash"/> </a>
                      </td>

                    </tr>
                  </c:forEach>
                </tbody>
              </table>
              <tag:paginate max="10" offset="${offset}" count="${count}" uri="inicio.htm" next="&raquo;" previous="&laquo;" />
            </div>
          </div>
        </div>
      </main>
    </div>
  </body>
</html>