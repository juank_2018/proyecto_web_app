<%-- 
    Document   : inicio
    Created on : 30-ene-2018, 11:54:38
    Author     : jcpm0
    Vista de inicio de la aplicación, muestra las reservas solicitadas por los clientes de la app
    recibe desde el controlador la variable reservas con la lista de reservas.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" uri="/WEB-INF/jsp/taglib/customTaglib.tld"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
  <head>
    <meta  charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-foundation.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/estilos.css" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.js" ></script>
    <script src="${pageContext.request.contextPath}/resources/js/popper.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.validate.js"></script>

    <title>Inicio</title>
  </head>
  <body>
    <div class="container ">
      <header class="row justify-content-md-endr ">
        <div class="col "><!-- columna única para la barra de navegación -->
          <nav class="navbar navbar-expand-sm navbar-dark bg-primary ">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/inicio">#BienMeSabe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
              <ul class="navbar-nav   ">
                <li class="nav-item">
                  <a class="nav-link  " href="${pageContext.request.contextPath}/empleados">Empleados</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/platos">Platos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/carta">Carta</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/menu">Menú del día</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="${pageContext.request.contextPath}/inicio">Reservas</a>
                </li>
                <li class="nav-item dropdown ">
                  <a class="nav-link dropdown-toggle" href="#" 
                     id="navbarDropdownPlatos" role="button" 
                     data-toggle="dropdown" aria-haspopup="true" 
                     aria-expanded="false">Notificaciones</a>
                  <div class="dropdown-menu" 
                       aria-labelledby="navbarDropdownPlatos">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificaciones">Sistema</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificacionesClientes">Cliente</a>

                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/spring/logout"  >Salir</a>
                </li>

              </ul>
            </div>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" href="#">     <sec:authorize access="isAuthenticated()">
                    usuario:  <sec:authentication property="principal.username" /> 
                  </sec:authorize></a>
              </li>
            </ul> 
          </nav>
        </div><!-- columna única para la barra de navegación -->
      </header>
      <main class="row justify-content-md-center ">
        <div class="col-md-8 ">
          <div class="card ">
            <h5 class="card-header">Reservas</h5>

            <div class="card-body">
              <table class="table-responsive-md table-sm table-hover table-striped">
                <thead>
                  <tr>
                    <th class="text-center" scope="row">Id</th>
                    <th class="text-center" scope="row">Comensales</th>
                    <th class="text-center" scope="row">Turno</th>
                    <th class="text-center" scope="row">Fecha</th>
                    <th class="text-center" scope="row">Hora</th>
                    <th class="text-center" scope="row">Clienteliente</th>
                      <sec:authorize access="hasRole('ROLE_ADMIN')">

                      <th class="text-center" scope="row">Eliminar</th>

                    </sec:authorize>

                    <th class="text-center" scope="row">Notificar</th>
                  </tr>
                </thead>
                <tbody>
                  <!-- Recprro el item reservas para popular la tabla -->
                  <c:forEach items="${reservas}" var="consulta">
                    <tr>
                      <th scope="row"><c:out value="${consulta.idreservas}"></c:out></th>
                      <td><c:out value="${consulta.nComensales}"></c:out></td>
                      <td><c:out value="${consulta.turno}"></c:out></td>
                        <td>
                          <!-- El campo fechahora del modelo representa la fecha y hora que ha solicitado el cliente
                          uso un formateador jslt para separar en dos campos fecha y hora-->
                        <fmt:formatDate type = "Date" dateStyle = "short" timeStyle = "short" value = "${consulta.fechaHora}" var="fecha" />
                        <c:out value="${fecha}"></c:out>
                        </td>
                        <td>
                        <fmt:formatDate type = "time"  dateStyle = "short" timeStyle = "short" value = "${consulta.fechaHora}" var="hora" />
                        <c:out value="${hora}"></c:out>
                        </td>
                        <td><c:out value="${consulta.cliente.email}"></c:out></td>
                      <sec:authorize access="hasRole('ROLE_ADMIN')">

                        <td class="text-center">
                          <a href="borraReserva/${consulta.idreservas}" class="btn btn-danger btn-sm"> <span class="oi oi-trash"/> </a>
                        </td>

                      </sec:authorize>

                      <td class="text-center">
                        <!-- Para el botón confirmar reserva utilizo un formulario oculto, que basicamente es el mismo que hay en
                        la página de crearReservaCliente y con el método onclick lo envío como post al contrlador de notificaciones para
                        que envíe una notificación al cliente como que la reserva esté confirmada y la agregue a la bbdd-->
                        <a  onclick="document.getElementById('logout-form').submit();" class="btn btn-primary btn-sm"><span class="oi oi-bell "></span></a>
                          <fmt:formatDate pattern = "yyyy-MM-dd" value = "${fechaActual}" var="ahora" />
                        <form id="logout-form" action="<c:url value="/crearNotificacionClientes"/>" method="post">
                          <input type="hidden" name="fecha" value="${ahora}"/>
                          <input type="hidden" name="clientes" value="${consulta.cliente.idclientes}"/>
                          <input type="hidden" name="mensaje" value="La reserva que has solicitado para el día ${fecha} a las ${hora} para ${consulta.nComensales}, está confirmada. Gracias por confiar en nosotros!!"/>
                        </form>
                      </td>
                    </tr>
                  </c:forEach>
                </tbody>
              </table>
              <tag:paginate max="10" offset="${offset}" count="${count}" uri="inicio.htm" next="&raquo;" previous="&laquo;" />
            </div>
          </div>
        </div>
      </main>
    </div>
  </body>
</html>