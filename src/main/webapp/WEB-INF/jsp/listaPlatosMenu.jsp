<%-- 
Document   : listaPlatosMenu
Created on : 09-mar-2018, 0:30:29
Author     : jcpm0
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tag" uri="/WEB-INF/jsp/taglib/customTaglib.tld"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Listado platos del menu</title>
    <meta  charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-foundation.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/estilos.css" type="text/css">
    <script src="resources/js/jquery-3.3.1.js" ></script>

    <script src="${pageContext.request.contextPath}/resources/js/popper.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.validate.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function () {
        $('#crearMenu').validate({
          rules: {
            noombre: {
              required: true
            }
          },
          messages: {
            noombre: {
              required: "El campo nombre es obligatorio"
            }
          },
          errorClass: "is-invalid"
        });
      });
    </script>

  </head>
  <body>
    <div class=" container"><!-- container -->
      <header class="row justify-content-md-endr ">
        <div class="col "><!-- columna única para la barra de navegación -->
          <nav class="navbar navbar-expand-sm navbar-dark bg-primary ">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/inicio">#BienMeSabe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
              <ul class="navbar-nav   ">
                <li class="nav-item">
                  <a class="nav-link  " href="${pageContext.request.contextPath}/empleados">Empleados</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link  " href="${pageContext.request.contextPath}/platos">Platos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link  " href="${pageContext.request.contextPath}/carta">Carta</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="${pageContext.request.contextPath}/menu">Menú del día</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/inicio">Reservas</a>
                </li>
                <li class="nav-item dropdown ">
                  <a class="nav-link dropdown-toggle" href="#" 
                     id="navbarDropdownPlatos" role="button" 
                     data-toggle="dropdown" aria-haspopup="true" 
                     aria-expanded="false">Notificaciones</a>
                  <div class="dropdown-menu" 
                       aria-labelledby="navbarDropdownPlatos">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificaciones">Sistema</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificacionesClientes">Cliente</a>

                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/spring/logout"  >Salir</a>
                </li>

              </ul>
            </div>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  <sec:authorize access="isAuthenticated()"  >
                    usuario:  <sec:authentication property="principal.username" /> 
                  </sec:authorize>
                </a>
              </li>
            </ul> 
          </nav>
        </div><!-- columna única para la barra de navegación -->
      </header>
      <main class="row justify-content-md-center ">
        <div class="col-md-auto">
          <div class="card  "><!-- card -->
            <div class="card-header"> <!-- card header -->
              <h5 >Menú del día ${menu.fecha} </h5>
            </div><!-- card header -->
            <div class="card-body"><!-- card boy -->
              <div class="row justify-content-around ">
                <div class="col-md-auto ">
                  <div class="row table-responsive-md justify-content-md-end"> <!-- Tabla -->
                    <div class="col-md-auto">
                      <table class="table table-hover table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Primeros</th>
                          </tr>
                        </thead>
                        <tbody>
                          <c:forEach items="${mhp}" var="c" >
                            <tr>
                              <c:set var= "tipo" value="${c.tipo}"/>
                              <c:if test="${tipo == 'Primero'}">
                                <td><c:out value="${c.plato.nombre}"></c:out></td>
                              </c:if>
                            </tr>
                          </c:forEach>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-auto"> 
                      <table class="table table-hover table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Segundos</th>
                          </tr>
                        </thead>
                        <tbody>
                          <c:forEach items="${mhp}" var="c" >
                            <tr>
                              <c:set var= "tipo1" value="${c.tipo}"/>
                              <c:if test="${tipo1 == 'Segundo'}">
                                <td><c:out value="${c.plato.nombre}"></c:out></td>
                              </c:if>
                            </tr>
                          </c:forEach>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-auto"> 
                      <table class="table table-hover table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Postres</th>
                          </tr>
                        </thead>
                        <tbody>
                          <c:forEach items="${mhp}" var="c" >
                            <tr>
                              <c:set var= "tipo2" value="${c.tipo}"/>
                              <c:if test="${tipo2 == 'Postre'}">
                                <td><c:out value="${c.plato.nombre}"></c:out></td>
                              </c:if>
                            </tr>
                          </c:forEach>
                        </tbody>
                      </table>
                    </div>   
                  </div><!-- Tabla -->  
                </div>
                <div class="col-md-10 ">
                  <div class="row justify-content-md-end ">
                    <div class="col-md-auto ">
                      <a href="${pageContext.request.contextPath}/menu"
                         class="btn btn-info btn-sm oi oi-pencil">
                        Volver </a>
                    </div>
                    <div class="col-md-auto">
                      <a href="${pageContext.request.contextPath}/editaMenu/${menu.idmenu}"
                         class="btn btn-info btn-sm oi oi-pencil">
                        Modificar </a>
                    </div>
                  </div>
                </div>               
              </div>
            </div>
          </div> <!-- card boy -->
        </div><!-- card -->
      </main>
    </div><!-- Fin contenedor -->
  </body>
</html>
