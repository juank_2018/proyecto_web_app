<%-- 
    Document   : notificacionesClientes
    Created on : 06-mar-2018, 18:50:44
    Author     : jcpm0
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tag" uri="/WEB-INF/jsp/taglib/customTaglib.tld"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Notificaciones</title>
    <meta  charset="UTF-8">
    <meta name="viewport" content="width=device-width,
          initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/open-iconic-foundation.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/estilos.css" type="text/css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.js" ></script>
    <script src="${pageContext.request.contextPath}/resources/js/popper.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery.validate.js"></script>


  </head>
  <body>
    <div class=" container "><!-- container -->
      <header class="row justify-content-md-endr ">
        <div class="col "><!-- columna única para la barra de navegación -->
          <nav class="navbar navbar-expand-sm navbar-dark bg-primary ">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/inicio">#BienMeSabe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" 
                    aria-controls="navbarSupportedContent" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse " id="navbarSupportedContent">
              <ul class="navbar-nav   ">
                <li class="nav-item">
                  <a class="nav-link  " href="${pageContext.request.contextPath}/empleados">Empleados</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/platos">Platos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link  " href="${pageContext.request.contextPath}/carta">Carta</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/menu">Menú del día</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " href="${pageContext.request.contextPath}/inicio">Reservas</a>
                </li>
                <li class="nav-item dropdown active">
                  <a class="nav-link dropdown-toggle" href="#" 
                     id="navbarDropdownPlatos" role="button" 
                     data-toggle="dropdown" aria-haspopup="true" 
                     aria-expanded="false">Notificaciones</a>
                  <div class="dropdown-menu" 
                       aria-labelledby="navbarDropdownPlatos">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificaciones">Sistema</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/notificacionesClientes">Cliente</a>

                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/spring/logout"  >Salir</a>
                </li>

              </ul>
            </div>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  <sec:authorize access="isAuthenticated()"  >
                    usuario:  <sec:authentication property="principal.username" /> 
                  </sec:authorize>
                </a>
              </li>
            </ul> 
          </nav>
        </div><!-- columna única para la barra de navegación -->
      </header>
      <main class="row justify-content-md-center ">
        <div class="row">
        </div>
        <div class="row ">
          <div class="col">
            <div class="card "><!-- card -->
              <div class="card-header"> <!-- card header -->
                <h5 >Notificaciones a clientes</h5>
              </div><!-- card header -->
              <div class="card-body"><!-- card boy -->
                <c:if test="${msg != null}" >
                  <p class="alert alert-danger"><c:out value="${msg}"></c:out></p>
                </c:if>
                <div class="row justify-content-md-end">
                  <div class="col-md-3 " >
                    <a href="crearNotificacionClientes"
                       class="btn btn-info btn-sm"><i class="oi oi-plus"></i>
                      Nueva notificación
                    </a>
                  </div>
                </div>
                <div class="row table-responsive-md"> <!-- Tabla -->
                  <table class="table table-sm table-hover table-striped">
                    <thead>
                      <tr>
                        <th scope="row">Fecha</th>
                        <th scope="row">e-mail</th>
                        <th scope="row">Nombre</th>
                        <th scope="row">Mensaje</th>
                        <th scope="row">Enviada</th>
                          <sec:authorize access="hasRole('ROLE_ADMIN')">
                          <th scope="row">Eliminar</th>
                          </sec:authorize>

                      </tr>
                    </thead>
                    <tbody>
                      <c:forEach items="${notif}" var="notif1" >
                        <tr>
                          <th scope="row"><c:out value="${notif1.notificacion.fecha}"></c:out> </th>
                          <td><c:out value="${notif1.cliente.email}"></c:out></td>
                          <td><c:out value="${notif1.cliente.usuario.nombre} ${notif1.cliente.usuario.apellido1}"></c:out></td>
                          <td><c:out value="${notif1.notificacion.mensaje}"></c:out></td>
                            <td>
                            <c:choose>
                              <c:when test="${notif1.notificacion.entregada == 1}">
                                <span class="oi oi-check "></span>                              
                              </c:when>
                              <c:otherwise >
                                <span class="oi oi-ban "></span>                              
                              </c:otherwise>
                            </c:choose>
                          </td>
                          <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <td><a href="borraNotificacionCliente/${notif1.notificacion.idnotificaciones}" class="btn btn-danger btn-sm">
                                <span class="oi oi-trash "></span>                              
                              </a>
                            </td>  
                          </sec:authorize>

                        </tr>
                      </c:forEach>
                    </tbody>
                  </table>
                </div><!-- Tabla -->
                <tag:paginate max="10" offset="${offset}" count="${count}"
                              uri="notificacionesClientes.htm" next="&raquo;" previous="&laquo;" />
              </div> <!-- card boy -->
            </div><!-- card -->
          </div><!-- col -->
        </div>
      </main>
    </div><!-- Fin contenedor -->
  </body>
</html>
