/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import spring.config.RootConfig;
import spring.config.WebSecurityConfig;

/**
 *
 * @author jcpm0
 */
public class SpringWebAppInitializerTest extends AbstractAnnotationConfigDispatcherServletInitializer {

  /**
   *
   * @return
   */
  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[]{RootConfig.class,WebSecurityConfig.class};
  }

  /**
   *
   * @return
   */
  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[]{WebConfigTest.class};
  }

  /**
   *
   * @return
   */
  @Override
  protected String[] getServletMappings() {
    return new String[]{"/"};
  }

}
