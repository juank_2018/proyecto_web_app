/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author jcpm0
 */
@Configuration
@ComponentScan(basePackages = {
  "spring.controller",
  "spring.DAO",
  "spring.model",
  "spring.service"
})
public class TestConfig {

}
