/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Config;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jboss.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.format.FormatterRegistry;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import org.springframework.web.servlet.view.InternalResourceViewResolver;
import spring.model.Carta;
import spring.model.Clientes;
import spring.utils.DateStringConverter;
import spring.utils.StringDateConverter;

/**
 *
 * @author jcpm0
 */
@Configuration
@EnableWebMvc
@PropertySource({"classpath:dbTest.properties",
  "classpath:mail.properties"})
@EnableTransactionManagement
@ComponentScan("spring")
public class WebConfigTest implements WebMvcConfigurer {

  Logger LOG = Logger.getLogger(WebConfigTest.class);

  /**
   *
   * @return
   */
  @Bean
  public ViewResolver viewResolver() {
    InternalResourceViewResolver resolver
            = new InternalResourceViewResolver();
    resolver.setPrefix("/WEB-INF/jsp/");
    resolver.setSuffix(".jsp");
    resolver.setExposeContextBeansAsAttributes(true);
    return resolver;
  }

  /**
   *
   * @param registry
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/css/**")
            .addResourceLocations("/resources/css")
            .setCachePeriod(31556926);
    registry.addResourceHandler("/js/**")
            .addResourceLocations("/resources/js")
            .setCachePeriod(31556926);
    registry.addResourceHandler("**")
            .addResourceLocations("/resources")
            .setCachePeriod(31556926);
  }

  /**
   *
   * @param configurer
   */
  @Override
  public void configureDefaultServletHandling(
          DefaultServletHandlerConfigurer configurer) {
    configurer.enable();
  }

  /**
   *
   * @param registry
   */
  @Override
  public void addFormatters(FormatterRegistry registry) {
    registry.addConverter(new StringDateConverter());
    registry.addConverter(new DateStringConverter());
  }
  @Autowired
  private Environment env;

  /**
   *
   * @return
   */
  @Bean
  public DataSource getDataSource() {
   // String url = "jdbc:h2:mem:test;INIT=runscript from  'classpath:init.sql'";
    LOG.error("Entra en datasource");
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName("org.h2.Driver");
    dataSource.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
    dataSource.setUsername("sa");
    dataSource.setPassword("");
    //"jdbc:h2:mem:test;DB_CLOSE_DELAY=-1"
    return dataSource;
  }

  /**
   *
   * @return
   */
  @Bean
  public LocalSessionFactoryBean sessionFactory() {
    LOG.error("entra en sessionFactoryTest");
    LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
    sessionFactory.setDataSource(getDataSource());

    Properties props = new Properties();
    props.put("hibernate.hbm2ddl.auto", "create-drop");
    props.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
    props.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
    props.put("hibernate.format_sql", "true");

    sessionFactory.setHibernateProperties(props);
   // sessionFactory.setAnnotatedClasses(Carta.class, Clientes.class);
    sessionFactory.setPackagesToScan("spring.model");
    return sessionFactory;
  }

  /**
   *
   * @return
   */
  @Bean
  public HibernateTransactionManager getTransactionManager() {
    HibernateTransactionManager transactionManager
            = new HibernateTransactionManager();
    transactionManager.setSessionFactory(sessionFactory().getObject());
    return transactionManager;
  }

  /**
   *
   * @return
   */
  @Bean
  public JavaMailSenderImpl mailSender() {
    JavaMailSenderImpl jm = new JavaMailSenderImpl();
    jm.setHost("smtp.gmail.com");
    jm.setUsername("pruebas.proyectoDAM@gmail.com");
    jm.setPassword("Pituitaria1");
    jm.setPort(587);
    Properties props = new Properties();
    props.put("mail.transport.protocol", env.getProperty("mail.transport.protocol"));
    props.put("mail.smtp.auth", env.getProperty("mail.smtp.auth"));
    props.put("mail.smtp.starttls.enable", env.getProperty("mail.smtp.starttls.enable"));
    props.put("mail.debug", env.getProperty("mail.debug"));
    props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
    jm.setJavaMailProperties(props);

    return jm;
  }
}
