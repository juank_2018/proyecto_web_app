/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.DAO;

import Config.SpringWebAppInitializerTest;
import Config.WebConfigTest;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import spring.config.RootConfig;
import spring.model.Carta;

/**
 *
 * @author jcpm0
 */
@WebAppConfiguration
@ContextConfiguration(classes = {SpringWebAppInitializerTest.class,
  WebConfigTest.class, RootConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class CartaDAOImplTest {

  @Autowired
  private CartaDao cartaDao;

  /**
   *
   */
  public CartaDAOImplTest() {
  }

  Carta c;

  List<Carta> lst;

  /**
   *
   */
  @Before
  public void setUp() {

    //defino una carta de prueba
    c = new Carta();
    c.setNoombre("Carta prueba");

  }

  /**
   *
   */
  @After
  public void tearDown() {
  }

  /**
   * Test of addCarta method, of class CartaDAOImpl.
   */
  @Test
  @Transactional
  public void testAddCarta() {

    //inserto la carta
    cartaDao.addCarta(c);
    //recupero la lista de cartas.
    lst = cartaDao.listCartas();
    //compruebo si se ha introducido
    assertEquals(c.getNoombre(), lst.get(lst.size() - 1).getNoombre());
  }

  /**
   * Test of addCarta method, of class CartaDAOImpl.
   */
  @Test
  @Transactional
  public void testAddCartaNull() {
    Carta carta = null;
    //inserto la carta
    try {
      cartaDao.addCarta(carta);
      fail("test fallido");
    } catch (Exception e) {
      assertTrue(true);

    }

  }

  /**
   * Test of updateCarta method, of class CartaDAOImpl.
   */
  @Test
  @Transactional
  public void testUpdateCarta() {

    //obtengo lista de cartas
    lst = cartaDao.listCartas();
    //modifico una carta
    Carta cartaModificada = lst.get(0);
    cartaModificada.setNoombre("OtraCarta");
    //actualizo
    cartaDao.updateCarta(cartaModificada);
    //vuelvo a obtener la lista
    lst = cartaDao.listCartas();
    //compruebo si se ha modificado
    assertEquals("OtraCarta", lst.get(0).getNoombre());

  }

  /**
   * Test of listCartas method, of class CartaDAOImpl.
   */
  @Test
  @Transactional
  public void testListCartas() {
    assertNotNull(cartaDao.listCartas());
  }

  /**
   * Test of getCartaById method, of class CartaDAOImpl.
   */
  @Test
  @Transactional
  public void testGetCartaById() {
    //localizamos la carta
    Carta c = cartaDao.getCartaById(1);
    //test ok si c no es nulo
    assertNotNull(c);
    //test ok si la carta se llama General
    assertEquals("General", c.getNoombre());
  }

  /**
   * Test of removeCarta method, of class CartaDAOImpl.
   */
  @Test
  @Transactional
  public void testRemoveCarta() {
    Carta c = null;
    cartaDao.removeCarta(1);
    try {
      c = cartaDao.getCartaById(1);
    } catch (Exception e) {
      assertNull(c);
    }
  }

}
