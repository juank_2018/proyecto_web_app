/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring.controller;

import Config.SpringWebAppInitializerTest;
import Config.WebConfigTest;
import org.hamcrest.Matchers;
import org.jboss.logging.Logger;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import spring.model.Carta;
import spring.utils.CartaPlato;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import spring.config.RootConfig;


@WebAppConfiguration
@ContextConfiguration(classes = {SpringWebAppInitializerTest.class,
  WebConfigTest.class,spring.config.WebSecurityConfig.class, RootConfig.class})
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class CartaControllerTest {
     private static final Logger LOG= 
          Logger.getLogger(CartaControllerTest.class.getName());
  @Autowired
  private WebApplicationContext ctx;
  
  private MockMvc mockMvc;
  
  @Autowired
  CartaController cartaController;
  
  /**
   *
   */
  @Before
  public void setUp() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx)
      .build();
  }
  
  /**
   *
   */
  public CartaControllerTest() {
  }

  /**
   * Test of list method, of class CartaController.
   * @throws java.lang.Exception
   */
  @Test
  public void testList() throws Exception {
     mockMvc.perform(get("/carta"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(view().name("carta"))
            .andExpect(model().attributeExists("cartas"))
            .andExpect(model().attributeExists("carta"))
             .andExpect(model().attributeExists("offset"))
             .andExpect(model().attributeExists("count"));
  }

  /**
   * Test of rellenaPlatos method, of class CartaController.
   */
  @Test
  public void testRellenaPlatos() {
    assertNotNull(cartaController.rellenaPlatos());
  }

  /**
   * Test of editaCarta method, of class CartaController.
   * @throws java.lang.Exception
   */
  @Test
  public void testEditaCarta() throws Exception {
    mockMvc.perform(get("/editaCarta/1"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(view().name("editaCarta"))
            .andExpect(model().attributeExists("carta"))
            .andExpect(model().attribute("carta", Matchers.any(Carta.class)))
            .andExpect(model().attributeExists("platosEnCarta"))
            .andExpect(model().attribute("platosEnCarta", Matchers.notNullValue()))
            .andExpect(model().attributeExists("nuevoPlatoCarta"))
            .andExpect(model().attribute("nuevoPlatoCarta", Matchers.any(CartaPlato.class)));
  }

  /**
   * Test of crearCarta method, of class CartaController.
   * @throws java.lang.Exception
   */
  @Test
  public void testCrearCarta() throws Exception {
  
    mockMvc.perform(MockMvcRequestBuilders.post("/crearCarta")
            .contentType(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED)
            .param("noombre", "prueba Integracion").accept(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED))
            .andExpect(view().name("carta"))
            .andDo(print())
            .andExpect(status().isOk());
    
  }

  /**
   * Test of delete method, of class CartaController.
   * @throws java.lang.Exception
   */
  @Test
  public void testDelete() throws Exception {
     mockMvc.perform(get("/borraCarta/1"))
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/carta"));
  }

  /**
   * Test of deletePlatoCarta method, of class CartaController.
   * @throws java.lang.Exception
   */
  @Test
  public void testDeletePlatoCarta() throws Exception {
     mockMvc.perform(get("/borraPlatoCarta/Lambrusco/1"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(view().name("/editaCarta"))
            .andExpect(model().attributeExists("carta"))
            .andExpect(model().attribute("carta", Matchers.any(Carta.class)))
            .andExpect(model().attributeExists("platosEnCarta"))
            .andExpect(model().attribute("platosEnCarta", Matchers.notNullValue()))
            .andExpect(model().attributeExists("nuevoPlatoCarta"))
            .andExpect(model().attribute("nuevoPlatoCarta", Matchers.any(CartaPlato.class)));
  }

  /**
   * Test of actualizaEstado method, of class CartaController.
   * @throws java.lang.Exception
   */
  @Test
  public void testActualizaEstado() throws Exception {
     mockMvc.perform(get("/actualizaEstado/Lambrusco/1/off"))
            .andDo(print())
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/editaCarta/{idcarta}"))
            .andExpect(model().attributeExists("carta"))
            .andExpect(model().attribute("carta", Matchers.any(Carta.class)))
            .andExpect(model().attributeExists("platosEnCarta"))
            .andExpect(model().attribute("platosEnCarta", Matchers.notNullValue()))
            .andExpect(model().attributeExists("nuevoPlatoCarta"))
            .andExpect(model().attribute("nuevoPlatoCarta", Matchers.any(CartaPlato.class)));
  }

  /**
   * Test of actualizaCarta method, of class CartaController.
   * @throws java.lang.Exception
   */
  @Test
  public void testActualizaCarta() throws Exception {
     mockMvc.perform(MockMvcRequestBuilders.post("/actualizaCarta")
            .contentType(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED)
            .param("idPlato", "71")
             .param("idCarta", "1")
             .param("aparece", "1")
             .accept(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED))
            .andExpect(view().name("/editaCarta"))
            .andDo(print())
            .andExpect(status().isOk());
  }
  
  /**
   *
   * @throws Exception
   */
  @Test
  public void testActualizaCartaPlatoExistente() throws Exception {
     mockMvc.perform(MockMvcRequestBuilders.post("/actualizaCarta")
            .contentType(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED)
            .param("idPlato", "70")
             .param("idCarta", "1")
             .param("aparece", "1")
             .accept(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED))
            .andExpect(view().name("/editaCarta"))
             .andExpect(model().attributeExists("msg"))
            .andDo(print())
            .andExpect(status().isOk());
  }


  private RequestBuilder post(String crearCarta) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
  
}
