/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package spring.web;
//
//import static org.hamcrest.CoreMatchers.any;
//import org.jboss.logging.Logger;
//import org.junit.Test;
//import org.junit.Before;
//import org.junit.runner.RunWith;
////import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.test.context.ContextConfiguration;
////import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
////import org.springframework.test.context.web.WebAppConfiguration;
////import org.springframework.test.web.servlet.MockMvc;
////import org.springframework.test.web.servlet.setup.MockMvcBuilders;
////import org.springframework.web.context.WebApplicationContext;
//import static org.springframework.test.web.servlet.request
//.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.
//MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.
//MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.
//MockMvcResultMatchers.view;
//import static org.springframework.test.web.servlet.result.
//MockMvcResultMatchers.*;
////import spring.model.Empleados;
////
/////**
//// *
// * @author jcpm0
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(classes = { spring.config.SpringWebAppInitializer.class,
//spring.config.WebConfig.class , spring.config.RootConfig.class})
//public class LoginControllerTest {
//  
////  /**
////   *
////   */
////  public LoginControllerTest() {
////  }
////
//   private static final Logger LOG= 
//          Logger.getLogger(LoginControllerTest.class.getName());
//  @Autowired
//  private WebApplicationContext ctx;
//  
//  private MockMvc mockMvc;
//  
//  /**
//   *
//   */
//  @Before
//  public void setUp() {
//    this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx)
//      .build();
//  }
////  /**
////   * Test of handleRequest method, of class LoginController.
////   * @throws java.lang.Exception
////   */
//  @Test
//  public void testHandleRequest() throws Exception {
//    mockMvc.perform(get("/"))
//            .andDo(print())
//            .andExpect(status().isOk())
//            .andExpect(view().name("login"))
//            .andExpect(model().attributeExists("empleado"))
//            .andExpect(model().attribute("empleado", any(Empleados.class)));   
//            
//  }

//  /**
//   *
//   * @throws Exception
//   */
//  @Test
//  public void testHandleRequestFail() throws Exception {
//    mockMvc.perform(get("kkif"))
//            .andDo(print())
//            .andExpect(status().isNotFound());
//            
//            
//  }
//
//  /**
//   * Test of doLogin method, of class LoginController.
//   */
//  @Test
//  public void testDoLogin() {
//  }
//  
//}
